package com.isuisudmt;

public class HomeContract {
    public interface View {
        void fetchedHomeDashboardResponse(boolean status, String response);
    }

    public interface UserInteraction{
        void getHomeDashboardResponse(String base_url);
    }
}
