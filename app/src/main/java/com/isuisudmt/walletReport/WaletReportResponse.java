package com.isuisudmt.walletReport;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WaletReportResponse {
    @SerializedName("BQReport")
    private ArrayList<WalletReportModel> mATMTransactionReport;

    public ArrayList<WalletReportModel> getmATMTransactionReport() {
        return this.mATMTransactionReport;
    }

    public void setmATMTransactionReport(ArrayList<WalletReportModel> mATMTransactionReport2) {
        this.mATMTransactionReport = mATMTransactionReport2;
    }
}
