package com.isuisudmt.report;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.report.activity.ActivityReportDashboardNew;
import com.isuisudmt.report.adapter.AdapterReportCommision2;
import com.isuisudmt.report.calender.CalenderActivity;
import com.isuisudmt.report.model.ModelReportCommision2;
import com.isuisudmt.report.util.ConstantsReport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static android.view.View.GONE;
import static com.isuisudmt.report.util.ConstantsReport.CALENDER_PICKER_CODE;
import static com.isuisudmt.utils.Constants.URL_COMMISION;


public class FragmentReportCommision2 extends Fragment {

    TextView noData;

    private RecyclerView reportRecyclerView;
    AdapterReportCommision2 mAdapter2;
    RecyclerView.LayoutManager layoutManager;
    AutoCompleteTextView transaction_spinner;
    ArrayList<ModelReportCommision2> commision2Reports = new ArrayList<>();

    SessionManager session;
    String tokenStr = "";
    ProgressBar progressV;
    Context context;
    String _userName;
    String fromdate = "", todate = "";
    Boolean calenderFlag = false;
    String reportType = "com2";

    TextView totalreport, amount;
    LinearLayout detailsLayout;

    DecimalFormat decim;


    public FragmentReportCommision2() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_commision, container, false);

        initView(rootView);

        if (calenderFlag) {
            //Do Nothing
        } else {
            callCalenderFunction();
        }

        return rootView;
    }

    private void initView(View rootView) {
        ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setTitle("Commission 2 Report");

        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        HashMap<String, String> user = session.getUserSession();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        _userName = user.get(SessionManager.userName);
        progressV = rootView.findViewById(R.id.progressV);
        noData = rootView.findViewById(R.id.noData);

        amount = rootView.findViewById(R.id.amount);
        totalreport = rootView.findViewById(R.id.totalreport);
        detailsLayout = rootView.findViewById(R.id.detailsLayout);

        transaction_spinner = rootView.findViewById(R.id.transaction_spinner);
        reportRecyclerView = rootView.findViewById(R.id.reportRecyclerView);
        reportRecyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        reportRecyclerView.setLayoutManager(layoutManager);

        decim = new DecimalFormat("0.00");

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        } else if (id == R.id.filterBar) {
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (commision2Reports != null && commision2Reports.size() > 0) {
                    mAdapter2.getFilter().filter(s);
                    mAdapter2.notifyDataSetChanged();
                } else {
                    if (calenderFlag == false)
                        Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
                }
                return false;
            }
        });

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }

    private void callcommition2(String fromdate, String nextDate, String tokenStr) {
        showLoader();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("startDate", fromdate);
            jsonObject.put("endDate", nextDate);
            jsonObject.put("userName", _userName);

            AndroidNetworking.post(URL_COMMISION)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization", tokenStr)
                    .addJSONObjectBody(jsonObject)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                hideLoader();

                                commision2Reports.clear();
                                JSONObject obj = new JSONObject(response.toString());
                                JSONArray jsonArray = obj.getJSONArray("BQReport");

                                if (jsonArray.length() == 0) {
                                    noData.setVisibility(View.VISIBLE);
                                    detailsLayout.setVisibility(View.GONE);
                                    reportRecyclerView.setVisibility(View.GONE);
                                } else {

                                    try {
                                        reportRecyclerView.setVisibility(View.VISIBLE);
                                        noData.setVisibility(GONE);

                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            noData.setVisibility(GONE);

                                            ModelReportCommision2 reportModel = new ModelReportCommision2();
                                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                            reportModel.setId(jsonObject1.getString("id"));
                                            reportModel.setRelationalId(jsonObject1.getString("relationalId"));
                                            reportModel.setAmountTransacted(jsonObject1.getString("amountTransacted"));
                                            reportModel.setPreviousAmount(jsonObject1.getString("previousAmount"));
                                            reportModel.setRelationalAmount(jsonObject1.getString("relationaAmount"));
                                            reportModel.setBalanceAmount(jsonObject1.getString("balanceAmount"));
                                            reportModel.setStatus(jsonObject1.getString("status"));
                                            reportModel.setCreditedUser(jsonObject1.getString("creditedUser"));
                                            reportModel.setDistributerName(jsonObject1.getString("distributerName"));
                                            reportModel.setMasterName(jsonObject1.getString("masterName"));
                                            reportModel.setTransactingUser(jsonObject1.getString("transactingUser"));
                                            reportModel.setRelationalOperation(jsonObject1.getString("relationalOperation"));
                                            reportModel.setaPI(jsonObject1.getString("API"));
                                            reportModel.setCreatedDate(jsonObject1.getLong("createdDate"));
                                            reportModel.setUpdatedDate(jsonObject1.getLong("updatedDate"));
                                            reportModel.setType(jsonObject1.getString("type"));


                                            commision2Reports.add(reportModel);
                                        }
                                        mAdapter2 = new AdapterReportCommision2(getActivity(), commision2Reports, amount, totalreport);
                                        reportRecyclerView.setAdapter(mAdapter2);
                                        hideLoader();
                                        Collections.reverse(commision2Reports);


                                        totalreport.setText("Entries: " + commision2Reports.size());
                                        double totalAmount = 0;
                                        for (int i = 0; i < commision2Reports.size(); i++) {
                                            if (!commision2Reports.get(i).getAmountTransacted().equalsIgnoreCase("null")) {
                                                totalAmount += Double.parseDouble(commision2Reports.get(i).getAmountTransacted());
                                            }
                                        }
                                        detailsLayout.setVisibility(View.VISIBLE);
                                        DecimalFormat f = new DecimalFormat("##.00");
                                        amount.setText(getResources().getString(R.string.toatlamountacitvity) + decim.format(totalAmount));

                                    } catch (Exception e) {

                                    }


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                noData.setVisibility(View.VISIBLE);

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.getErrorDetail();
                            transaction_spinner.setVisibility(GONE);
                            if (commision2Reports.size() <= 0) {
                                noData.setVisibility(View.VISIBLE);
                            } else {
                                noData.setVisibility(GONE);
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showLoader() {
        progressV.setVisibility(View.VISIBLE);
    }

    private void hideLoader() {
        progressV.setVisibility(GONE);
    }

    private void callCalenderFunction() {
        Intent intent = new Intent(getActivity(), CalenderActivity.class);
        startActivityForResult(intent, CALENDER_PICKER_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CALENDER_PICKER_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectToolbarFromDate + " to " + ConstantsReport.selectToolbarToDate);
                calenderFlag = true;

                //Make API call
                callcommition2(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate), tokenStr);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle("");
                calenderFlag = false;
                //Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
