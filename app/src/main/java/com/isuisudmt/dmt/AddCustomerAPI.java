package com.isuisudmt.dmt;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface AddCustomerAPI {
    @POST()
//    @Headers("Content-Type: application/json")
    Call<AddCustomerResponse> getAddCustomer(@Header("Authorization") String token, @Body AddCustomerRequest addCustomerRequestBody, @Url String url);
}
