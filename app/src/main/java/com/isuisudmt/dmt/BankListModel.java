package com.isuisudmt.dmt;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class BankListModel implements Parcelable, Serializable {

    private String ACCOUNT_VERAFICATION_AVAILABLE;

    private String PATTERN;

    private String BANKCODE;

    private String IMPS_AVAILABILITY;

    private String NEFT_AVAILABILITY;

    private String BANKNAME;

    private String FLAG;

    private Boolean active;

    public static final Creator<BankListModel> CREATOR = new Creator<BankListModel>() {
        @Override
        public BankListModel createFromParcel(Parcel in) {
            return new BankListModel(in);
        }

        @Override
        public BankListModel[] newArray(int size) {
            return new BankListModel[size];
        }
    };

    public String getACCOUNT_VERAFICATION_AVAILABLE() {
        return ACCOUNT_VERAFICATION_AVAILABLE;
    }

    public void setACCOUNT_VERAFICATION_AVAILABLE(String ACCOUNT_VERAFICATION_AVAILABLE) {
        this.ACCOUNT_VERAFICATION_AVAILABLE = ACCOUNT_VERAFICATION_AVAILABLE;
    }

    public String getPATTERN() {
        return PATTERN;
    }

    public void setPATTERN(String PATTERN) {
        this.PATTERN = PATTERN;
    }

    public String getBANKCODE() {
        return BANKCODE;
    }

    public void setBANKCODE(String BANKCODE) {
        this.BANKCODE = BANKCODE;
    }

    public String getIMPS_AVAILABILITY() {
        return IMPS_AVAILABILITY;
    }

    public void setIMPS_AVAILABILITY(String IMPS_AVAILABILITY) {
        this.IMPS_AVAILABILITY = IMPS_AVAILABILITY;
    }

    public String getNEFT_AVAILABILITY() {
        return NEFT_AVAILABILITY;
    }

    public void setNEFT_AVAILABILITY(String NEFT_AVAILABILITY) {
        this.NEFT_AVAILABILITY = NEFT_AVAILABILITY;
    }

    public String getBANKNAME() {
        return BANKNAME;
    }

    public void setBANKNAME(String BANKNAME) {
        this.BANKNAME = BANKNAME;
    }

    public String getFLAG() {
        return FLAG;
    }

    public void setFLAG(String FLAG) {
        this.FLAG = FLAG;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }


    public BankListModel()
    {

    }

    private BankListModel(Parcel in) {
        ACCOUNT_VERAFICATION_AVAILABLE = in.readString();
        PATTERN = in.readString();
        BANKCODE = in.readString();
        IMPS_AVAILABILITY = in.readString();
        NEFT_AVAILABILITY = in.readString();
        BANKNAME = in.readString();
        FLAG = in.readString();

    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ACCOUNT_VERAFICATION_AVAILABLE);
        dest.writeString(PATTERN);
        dest.writeString(BANKCODE);
        dest.writeString(IMPS_AVAILABILITY);
        dest.writeString(NEFT_AVAILABILITY);
        dest.writeString(BANKNAME);
        dest.writeString(FLAG);
    }
}
