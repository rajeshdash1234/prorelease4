package com.isuisudmt.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.Util;
import com.isuisudmt.report.model.ModelReportCommision;
import com.paxsz.easylink.model.AppSelectResponse;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class AdapterReportCommision extends RecyclerView.Adapter<AdapterReportCommision.ViewHolder> implements Filterable {

    private int lastSelectedPosition = -1;
    private Context context;
    private List<ModelReportCommision> commisionReports = new ArrayList<>();
    private List<ModelReportCommision> commisionReportsFiltered = new ArrayList<>();
    TextView fragAmount, fragTotal;

    public AdapterReportCommision(Context context, List finoTransactionReports, TextView fragAmount, TextView fragTotal) {
        this.context = context;
        this.commisionReports = finoTransactionReports;
        this.commisionReportsFiltered = finoTransactionReports;
        this.fragTotal = fragTotal;
        this.fragAmount = fragAmount;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.commision_report_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.pre_amount.setVisibility(View.GONE);
        holder.bal_amount.setVisibility(View.GONE);
        holder.relational_op.setVisibility(View.GONE);
        holder.relational_id.setVisibility(View.GONE);
        holder.relational_amount.setVisibility(View.GONE);

        ModelReportCommision current = commisionReportsFiltered.get(position);

        if (current.getAmountTransacted().toString().equals("null") || current.getAmountTransacted().toString().equals(null) || current.getAmountTransacted().toString().equals(""))
            holder.amount_transacted_recharge.setText(context.getResources().getString(R.string.toatlamountreport) + "N/A");
        else
            holder.amount_transacted_recharge.setText(context.getResources().getString(R.string.toatlamountreport) + current.getAmountTransacted());

        holder.updated_date.setText(new StringBuilder().append("Date: ").append(Util.getDateTime(String.valueOf(current.getUpdatedDate()))).toString());

        if (current.getStatus().toString().equals("null") || current.getStatus().toString().equals(null) || current.getStatus().toString().equals(""))
            holder.status.setText("N/A");
        else
            holder.status.setText(current.getStatus());


        if (current.getId().toString().equals("null") || current.getId().toString().equals(null) || current.getId().toString().equals(""))

            holder.transaction_id_recharge.setText("Transaction Id: " + "N/A");
        else
            holder.transaction_id_recharge.setText(new StringBuilder().append("Transaction Id: ").append(String.valueOf(current.getId())).toString());

        if (current.getgST().toString().equals("null") || current.getgST().toString().equals(null) || current.getgST().toString().equals(""))
            holder.mobile_number.setText("GST : " + "N/A");
        else
            holder.mobile_number.setText(new StringBuilder().append("GST : ").append(current.getgST()).toString());

        if (current.gettDS().toString().equals("null") || current.gettDS().toString().equals(null) || current.gettDS().toString().equals(""))
            holder.operator_performed.setText("TDS :" + "N/A");
        else
            holder.operator_performed.setText("TDS :" + current.gettDS());
        if (current.getTaxable().toString().equals("null") || current.getTaxable().toString().equals(null) || current.getTaxable().toString().equals(""))
            holder.transaction_mode.setText("Taxable :" + "N/A");
        else
            holder.transaction_mode.setText("Taxable :" + current.getTaxable());

        if (current.getInvoiceValue().toString().equals("null") || current.getInvoiceValue().toString().equals(null) || current.getInvoiceValue().toString().equals(""))
            holder.invoiceValue.setText("Invoice Value: " + "N/A");
        else
            holder.invoiceValue.setText("Invoice Value: " + current.getInvoiceValue());


        if (current.getStatus().equals(AppSelectResponse.SUCCESS)) {
            holder.status.setTextColor(ContextCompat.getColor(this.context, R.color.color_report_green));
        } else {
            holder.status.setTextColor(ContextCompat.getColor(this.context, R.color.red));
        }
    }

    @Override
    public int getItemCount() {
        return commisionReportsFiltered.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView amount_transacted_recharge;
        public TextView mobile_number;
        public TextView operator_performed;
        public TextView status;
        public TextView transaction_id_recharge;
        public TextView transaction_mode;
        public TextView updated_date;
        public TextView invoiceValue, pre_amount, bal_amount, relational_op, relational_id, relational_amount;

        public ViewHolder(View itemView) {
            super(itemView);
            updated_date = itemView.findViewById(R.id.updated_date);
            status = itemView.findViewById(R.id.status);
            transaction_id_recharge = itemView.findViewById(R.id.transaction_id_recharge);
            amount_transacted_recharge = itemView.findViewById(R.id.amount_transacted);
            mobile_number = itemView.findViewById(R.id.mobile_number);
            operator_performed = itemView.findViewById(R.id.operation_performed);
            transaction_mode = itemView.findViewById(R.id.transactionMode);
            invoiceValue = itemView.findViewById(R.id.invoiceValue);

            pre_amount = itemView.findViewById(R.id.pre_amount);

            bal_amount = itemView.findViewById(R.id.bal_amount);

            relational_op = itemView.findViewById(R.id.relational_op);

            relational_id = itemView.findViewById(R.id.relational_id);
            relational_amount = itemView.findViewById(R.id.relational_amount);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    commisionReportsFiltered = commisionReports;
                } else {
                    List<ModelReportCommision> filteredList = new ArrayList<>();
                    for (ModelReportCommision row : commisionReports) {

                        if (String.valueOf(row.getId()).toLowerCase().contains(charString.toLowerCase()) || String.valueOf(row.getId()).contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    commisionReportsFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = commisionReportsFiltered;

                getTotalAmount();

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                commisionReportsFiltered = (ArrayList<ModelReportCommision>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public double getTotalAmount() {
        double totalAmount = 0;
        DecimalFormat decim = new DecimalFormat("0.00");

        for (int i = 0; i < commisionReportsFiltered.size(); i++) {
            if (commisionReportsFiltered.get(i).getAmountTransacted().equals("N/A") || commisionReportsFiltered.get(i).getAmountTransacted().equals("null") ||
                    commisionReportsFiltered.get(i).getAmountTransacted().toString().equals(null)) {
                totalAmount += 0;
            } else {
                totalAmount += Double.parseDouble(String.valueOf(commisionReportsFiltered.get(i).getAmountTransacted()));
            }
        }

        fragAmount.setText(context.getResources().getString(R.string.toatlamountacitvity) + decim.format(totalAmount));
        fragTotal.setText("Entries: " + commisionReportsFiltered.size());

        return totalAmount;
    }
}
