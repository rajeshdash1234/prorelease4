package com.isuisudmt;


import android.util.Base64;
import android.util.Log;

import com.isuisudmt.retrofitService.DMTRetrofitInstance;
import com.isuisudmt.retrofitService.GetDataService;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePresenter implements HomeContract.UserInteraction {

    HomeContract.View view;

     public HomePresenter(HomeContract.View view){
        this.view = view;
    }

    @Override
    public void getHomeDashboardResponse(String base_url) {

        GetDataService getDataService = DMTRetrofitInstance.getretrofitInstance(base_url).create(GetDataService.class);

        getDataService.getV2().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.isSuccessful()){

                    try {
                        String res = response.body().string();

                        Log.d("v2 res ::::::",res);

                        JSONObject jsonObject = new JSONObject(res);
                        String url = jsonObject.getString("hello");

                        byte[] data = Base64.decode(url, Base64.DEFAULT);
                        String base64 = new String(data, "UTF-8");

                        Log.d("v2 base64 ::::::",base64);

                        view.fetchedHomeDashboardResponse(true, base64);

                    }catch (Exception exc){
                        exc.getMessage();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i(" ResponseBody v2 ::::::",":::::::: ResponseBody v2 :::::");
            }
        });
    }
}
