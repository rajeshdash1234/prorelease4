package com.isuisudmt.recharge;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.isuisudmt.R;

public class ReacargeStatusActivity extends AppCompatActivity {

    TextView title, message, close;
    LinearLayout status_success, status_failed, home;
    LinearLayout recharge_ll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reacarge_status);

        title = findViewById(R.id.title);
        message = findViewById(R.id.message);
        close = findViewById(R.id.close);
        recharge_ll = (LinearLayout) findViewById(R.id.recharge_ll);

        status_success = findViewById(R.id.status_success);
        status_failed = findViewById(R.id.status_faield);
        home = findViewById(R.id.home);


        String _title = getIntent().getStringExtra("title");
        String status = getIntent().getStringExtra("status");
        String _message = getIntent().getStringExtra("message");

        title.setText(_title);
        message.setText(_message);

        if (status.equals("true")) {
            status_success.setVisibility(View.VISIBLE);
            recharge_ll.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        } else {
            status_failed.setVisibility(View.VISIBLE);
            recharge_ll.setBackgroundColor(getResources().getColor(R.color.red));
        }

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}
