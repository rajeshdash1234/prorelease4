package com.isuisudmt.insurance;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog.Builder;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.load.Key;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.common.net.HttpHeaders;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.isuisudmt.CustomThemes;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

//import com.google.firebase.crashlytics.internal.analytics.AnalyticsConnectorReceiver;

public class OneLakhINR_insuranceActivity extends AppCompatActivity {
    TextInputLayout Layout_customer_system_nm;
    String _admin;
    String _token;
    TextInputEditText aadhar_no;
    ArrayAdapter<String> adapter;
    EditText address_one;
    EditText address_two;
    TextInputEditText age;
    int age_calulate = 0;
    String areaNm;
    String[] area_nm_arr;
    Spinner area_nm_spinner;
    Button cal_premium_submit;
    String cus_dob_selected;
    String customerDOBConvert_str;
    String customerSalutation;
    String customer_DOB_decrease_str;
    LinearLayout customer_LL;
    int customer_age = 0;
    LinearLayout customer_demographics_LL;
    String customer_existing_nm;
    String customer_genderType;
    TextInputEditText customer_nm;
    ArrayAdapter<String> customer_salutation_adapter;
    Spinner customer_salutation_spinner;
    TextInputEditText customer_system_nm;
    TextView dob;
    TextView dob_nominee;
    String dob_selected;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    EditText email_id;
    TextInputEditText first_nm_adhar;
    TextInputEditText first_nm_nominee;
    TextInputEditText gender;
    private Handler handler = new Handler();
    TextView heading_txt;
    TextView insured_amount_txt;
    List<String> itemList;
    String jobID;
    JSONObject lastData_json;
    String last_data_URL;
    TextInputEditText last_nm_adhar;
    TextInputEditText last_nm_nominee;
    String mobile_no;
    TextInputEditText mobile_txt;
    TextInputLayout mobile_txt_inp_lay;
    Calendar myCalendar;
    String nomineeDOBConvert_str;
    int nominee_age = 0;
    String nominee_dob_selected;
    String nominee_genderType;
    ArrayAdapter<String> nominee_relationAdapter;
    Spinner nominee_relation_spinner;
    String nominee_relation_str;
    String nominee_salutation;
    ArrayAdapter<String> nominee_salutation_adapter;
    Spinner nominee_salutation_spinner;
    String nominee_selected_genderType;

    /* renamed from: pd */
    ProgressDialog pd;
    EditText pin_edit;
    String premiumAmount;
    String premium_calculate_URL;
    String premium_total_amount;
    RadioGroup rg_customer_gender_type;
    RadioGroup rg_trans_type_nominee;
    String selected_genderType;
    Button send_otp_debit_submit;
    SessionManager session;
    ScrollView snackbar_ll;
    String status_desc="", GET_LOAD_WALLET_URL = "https://dmt.iserveu.tech/generate/v20";;
    TextView term_amount;
    private FirebaseAnalytics firebaseAnalytics;

    public OneLakhINR_insuranceActivity() {
        String str = "Male";
        this.customer_genderType = str;
        String str2 = "M";
        this.selected_genderType = str2;
        this.nominee_genderType = str;
        this.nominee_selected_genderType = str2;
        String str3 = "";
        this._token = str3;
        this._admin = str3;
        this.mobile_no = str3;
        this.areaNm = str3;
        this.customer_existing_nm = str3;
        this.status_desc = str3;
        this.jobID = str3;
        this.premiumAmount = str3;
        this.premium_total_amount = "100000";
        this.cus_dob_selected = str3;
        this.dob_selected = str3;
        this.nominee_dob_selected = str3;
        this.customerDOBConvert_str = str3;
        this.nomineeDOBConvert_str = str3;
        this.customerSalutation = str3;
        this.nominee_salutation = str3;
        this.nominee_relation_str = str3;
        this.customer_DOB_decrease_str = str3;
        this.area_nm_arr = new String[]{"Select location", "AndamanNicrobar", "AndhraPradesh", "ArunachalPradesh", "Assam", "Bangalore", "Bihar", "Chennai", "Chhattisgarh", "DadraandNagarHaveli", "DamanandDiu", "Delhi", "Goa", "Gujarat", "Haryana", "HimachalPradesh", "JammuAndKashmir", "Jharkhand", "Karnataka", "Kerala", "Kolkata", "MadhyaPradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Mumbai", "Nagaland", "Orissa", "Pondicherry", "Punjab", "Rajasthan", "Sikkim", "TamilNadu", "Tripura", "Uttar Pradesh East", "Uttar Pradesh West", "Uttrakhand", "West Bengal"};
        this.lastData_json = new JSONObject();
        this.premium_calculate_URL = "https://itpl.iserveu.tech/generate/v91";
        this.last_data_URL = "https://itpl.iserveu.tech/generate/v92";
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);
        setContentView(R.layout.one_two_lakh_inr_insurance_layout);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        getViewInitialize();
        pd = new ProgressDialog(OneLakhINR_insuranceActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle((CharSequence) getResources().getString(R.string.insurance));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
            }
        });
        session = new SessionManager(OneLakhINR_insuranceActivity.this);
        HashMap<String, String> user = this.session.getUserDetails();
        _token = (String) user.get(SessionManager.KEY_TOKEN);
        _admin = (String) user.get(SessionManager.KEY_ADMIN);

        mobile_txt.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                if (mobile_txt.getText().toString().trim().length() == 10) {
                    mobile_txt_inp_lay.setError(null);
                    mobile_no = mobile_txt.getText().toString().trim();
                    getCustomerApi(mobile_no);
                }
                else {
                    mobile_txt_inp_lay.setError("Enter 10 digit mobile no.");
                    customer_LL.setVisibility(View.GONE);
                    customer_demographics_LL.setVisibility(View.GONE);
                }
            }
        });
        rg_customer_gender_type.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_be) {
                    OneLakhINR_insuranceActivity.this.customer_genderType = "Female";
                } else if (checkedId == R.id.rb_cw) {
                    OneLakhINR_insuranceActivity.this.customer_genderType = "Male";
                }
            }
        });
        rg_trans_type_nominee.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_be_nominee) {
                    OneLakhINR_insuranceActivity.this.nominee_genderType = "Female";
                } else if (checkedId == R.id.rb_cw_nominee) {
                    OneLakhINR_insuranceActivity.this.nominee_genderType = "Male";
                }
            }
        });
        dob_nominee.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                OneLakhINR_insuranceActivity.this.myCalendar = Calendar.getInstance();
                OnDateSetListener date = new OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        myCalendar.set(5, dayOfMonth);
                        myCalendar.set(2, monthOfYear);
                        myCalendar.set(1, year);
                        updateLabel(dayOfMonth, monthOfYear, year, "nominee_dob");
                    }
                };
                DatePickerDialog datePickerDialog = new DatePickerDialog(OneLakhINR_insuranceActivity.this, date,
                        myCalendar.get(1),
                        myCalendar.get(2),
                        myCalendar.get(5));
                datePickerDialog.show();
            }
        });
        dob.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                OneLakhINR_insuranceActivity.this.myCalendar = Calendar.getInstance();
                OnDateSetListener date = new OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String str = "customer_dob";
                        updateLabel(dayOfMonth, monthOfYear, year, str);
                        updatedecreaseDate_forcalculatePremiumAPi(dayOfMonth, monthOfYear, year, str);
                    }
                };
                DatePickerDialog datePickerDialog = new DatePickerDialog(OneLakhINR_insuranceActivity.this, date, OneLakhINR_insuranceActivity.this.myCalendar.get(1), OneLakhINR_insuranceActivity.this.myCalendar.get(2), OneLakhINR_insuranceActivity.this.myCalendar.get(5));
                datePickerDialog.show();
            }
        });
        itemList = new ArrayList(Arrays.asList(area_nm_arr));
        adapter = new ArrayAdapter<String>(OneLakhINR_insuranceActivity.this, android.R.layout.simple_list_item_1, itemList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        area_nm_spinner.setAdapter(adapter);
        area_nm_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                OneLakhINR_insuranceActivity.this.areaNm = parent.getItemAtPosition(pos).toString();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        if (selected_genderType.equalsIgnoreCase("M")) {
            customer_salutation_adapter = new ArrayAdapter<>(OneLakhINR_insuranceActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.male_salutation_arr));
        } else {
            customer_salutation_adapter = new ArrayAdapter<>(OneLakhINR_insuranceActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.female_salutation_arr));
        }
        this.customer_salutation_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.customer_salutation_spinner.setAdapter(this.customer_salutation_adapter);
        this.customer_salutation_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                customerSalutation = parent.getItemAtPosition(pos).toString();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        nominee_salutation_adapter = new ArrayAdapter<>(OneLakhINR_insuranceActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.nomimee_salutation_arr));
        nominee_salutation_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        nominee_salutation_spinner.setAdapter(this.nominee_salutation_adapter);
        nominee_salutation_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                OneLakhINR_insuranceActivity.this.nominee_salutation = parent.getItemAtPosition(pos).toString();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        nominee_relationAdapter = new ArrayAdapter<>(OneLakhINR_insuranceActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.nominee_relations));
        nominee_relationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        nominee_relation_spinner.setAdapter(this.nominee_relationAdapter);
        nominee_relation_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                OneLakhINR_insuranceActivity.this.nominee_relation_str = parent.getItemAtPosition(pos).toString();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        cal_premium_submit.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String str = "";
                if (OneLakhINR_insuranceActivity.this.customer_nm.getText().toString().trim().equalsIgnoreCase(str)) {
                    Snackbar.make(snackbar_ll, (CharSequence) "Please enter customer name !", Snackbar.LENGTH_LONG).show();
                } else if (OneLakhINR_insuranceActivity.this.dob.getText().toString().trim().equalsIgnoreCase("MM/DD/YYYY")) {
                    Snackbar.make(snackbar_ll, (CharSequence) "Please select date of birth !", Snackbar.LENGTH_LONG).show();
                } else if (OneLakhINR_insuranceActivity.this.customer_genderType.equalsIgnoreCase(str)) {
                    Snackbar.make(snackbar_ll, (CharSequence) "Please select gender", Snackbar.LENGTH_LONG).show();
                } else {
                    if (customer_genderType.equalsIgnoreCase("Male")) {
                        OneLakhINR_insuranceActivity oneLakhINR_insuranceActivity = OneLakhINR_insuranceActivity.this;
                        String str2 = "M";
                        selected_genderType = str2;
                        gender.setText(str2);
                        customer_salutation_adapter = new ArrayAdapter<>(OneLakhINR_insuranceActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.male_salutation_arr));
                        customer_salutation_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        customer_salutation_spinner.setAdapter(customer_salutation_adapter);
                    } else {
                        String str3 = "F";
                        selected_genderType = str3;
                        gender.setText(str3);
                        customer_salutation_adapter = new ArrayAdapter<>(OneLakhINR_insuranceActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.female_salutation_arr));
                        customer_salutation_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        customer_salutation_spinner.setAdapter(OneLakhINR_insuranceActivity.this.customer_salutation_adapter);
                    }
                    customer_salutation_adapter.notifyDataSetChanged();

                    setFBAnalytics("ONE_LAKH_CAL_PREMIUM_SUBMIT", com.isuisudmt.Constants.USER_NAME);
                    getDynamicEncodedURL(premium_calculate_URL, "premium_calculate", selected_genderType, customer_DOB_decrease_str);
                    if (customer_existing_nm.equalsIgnoreCase(str)) {
                        customer_system_nm.setText(customer_nm.getText().toString().trim());
                    } else {
                        customer_system_nm.setText(customer_existing_nm);
                    }
                    TextInputEditText textInputEditText = age;
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append(OneLakhINR_insuranceActivity.this.age_calulate);
                    textInputEditText.setText(sb.toString());
                    customer_demographics_LL.setVisibility(View.VISIBLE);
                    customer_LL.setVisibility(View.GONE);
                }
            }
        });
        send_otp_debit_submit.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String str = "";
                if (first_nm_adhar.getText().toString().trim().equalsIgnoreCase(str)) {
                    Snackbar.make(snackbar_ll, (CharSequence) "Please enter customer first name as per adhaar!", Snackbar.LENGTH_LONG).show();
                } else if (last_nm_adhar.getText().toString().trim().equalsIgnoreCase(str)) {
                    Snackbar.make(snackbar_ll, (CharSequence) "Please enter customer last name as per adhaar!", Snackbar.LENGTH_LONG).show();
                } else {
                    String str2 = "Select title";
                    if (customerSalutation.equalsIgnoreCase(str2)) {
                        Snackbar.make(snackbar_ll, (CharSequence) "Please select customer salutation !", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                    if (!email_id.getText().toString().trim().equalsIgnoreCase(str)) {
                        if (validEmail(email_id.getText().toString().trim())) {
                            if (address_one.getText().toString().trim().equalsIgnoreCase(str)) {
                                Snackbar.make(snackbar_ll, (CharSequence) "Please enter address 1 !", Snackbar.LENGTH_LONG).show();
                                return;
                            } else if (address_two.getText().toString().trim().equalsIgnoreCase(str)) {
                                Snackbar.make(snackbar_ll, (CharSequence) "Please enter address 2 !", Snackbar.LENGTH_LONG).show();
                                return;
                            } else if (areaNm.equalsIgnoreCase("Select location")) {
                                Snackbar.make(snackbar_ll, (CharSequence) "Please select a location !", Snackbar.LENGTH_LONG).show();
                                return;
                            } else if (pin_edit.getText().toString().trim().equalsIgnoreCase(str) || OneLakhINR_insuranceActivity.this.pin_edit.getText().toString().trim().length() != 6) {
                                Snackbar.make(snackbar_ll, (CharSequence) "Please enter valid pin !", Snackbar.LENGTH_LONG).show();
                                return;
                            } else if (aadhar_no.getText().toString().trim().equalsIgnoreCase(str) || OneLakhINR_insuranceActivity.this.aadhar_no.getText().toString().trim().length() < 10) {
                                Snackbar.make(snackbar_ll, (CharSequence) "Please enter customer valid aadhar/pan no !", Snackbar.LENGTH_LONG).show();
                                return;
                            } else if (nominee_salutation.equalsIgnoreCase(str2)) {
                                Snackbar.make(snackbar_ll, (CharSequence) "Please select nominee salutation !", Snackbar.LENGTH_LONG).show();
                                return;
                            } else if (first_nm_nominee.getText().toString().trim().equalsIgnoreCase(str)) {
                                Snackbar.make(snackbar_ll, (CharSequence) "Please enter nominee first name !", Snackbar.LENGTH_LONG).show();
                                return;
                            } else if (last_nm_nominee.getText().toString().trim().equalsIgnoreCase(str)) {
                                Snackbar.make(snackbar_ll, (CharSequence) "Please enter nominee last name !", Snackbar.LENGTH_LONG).show();
                                return;
                            } else if (dob_nominee.getText().toString().trim().equalsIgnoreCase("MM/DD/YYYY")) {
                                Snackbar.make(snackbar_ll, (CharSequence) "Please select nominee date of birth !", Snackbar.LENGTH_LONG).show();
                                return;
                            } else if (nominee_relation_str.equalsIgnoreCase("Select Relation")) {
                                Snackbar.make(snackbar_ll, (CharSequence) "Please select nominee relation !", Snackbar.LENGTH_LONG).show();
                                return;
                            } else if (nominee_selected_genderType.equalsIgnoreCase(str)) {
                                Snackbar.make(snackbar_ll, (CharSequence) "Please select nominee gender", Snackbar.LENGTH_LONG).show();
                                return;
                            } else {
                                setFBAnalytics("ONE_LAKH_SEND_OTP_DEBIT_SUBMIT", com.isuisudmt.Constants.USER_NAME);
                                if (nominee_genderType.equalsIgnoreCase("Male")) {
                                    nominee_selected_genderType = "M";
                                } else {
                                   nominee_selected_genderType = "F";
                                }
                                String str3 = jobID;
                                String trim = first_nm_adhar.getText().toString().trim();
                                String trim2 = last_nm_adhar.getText().toString().trim();
                                String str4 = customerSalutation;
                                StringBuilder sb = new StringBuilder();
                                sb.append(str);
                                sb.append(customer_age);
                                lastData_json = getlast_data_json(str3, "", trim, trim2, str4, sb.toString(), selected_genderType, email_id.getText().toString().trim(),
                                        address_one.getText().toString().trim(), address_two.getText().toString().trim(), areaNm,
                                        pin_edit.getText().toString().trim(), nominee_salutation, first_nm_nominee.getText().toString().trim(),
                                        last_nm_nominee.getText().toString().trim(), nominee_selected_genderType, nomineeDOBConvert_str,
                                        customerDOBConvert_str, nominee_relation_str, premium_total_amount, mobile_no, "100", "");
                                getDynamicEncodedURL(last_data_URL, "submit_total_data", nominee_selected_genderType, nominee_dob_selected);
                                return;
                            }
                        }
                    }
                    Snackbar.make(snackbar_ll, (CharSequence) "Please enter valid email !", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    public void getViewInitialize() {
        customer_LL = (LinearLayout) findViewById(R.id.customer_LL);
        customer_demographics_LL = (LinearLayout) findViewById(R.id.customer_demographics_LL);
        snackbar_ll = (ScrollView) findViewById(R.id.snackbar_ll);
        mobile_txt_inp_lay = (TextInputLayout) findViewById(R.id.layout_mobile);
        mobile_txt = (TextInputEditText) findViewById(R.id.mobile);
        customer_nm = (TextInputEditText) findViewById(R.id.customer_nm);
        Layout_customer_system_nm = (TextInputLayout) findViewById(R.id.Layout_customer_system_nm);
        customer_system_nm = (TextInputEditText) findViewById(R.id.customer_system_nm);
        first_nm_adhar = (TextInputEditText) findViewById(R.id.first_nm_adhar);
        last_nm_adhar = (TextInputEditText) findViewById(R.id.last_nm_adhar);
        age = (TextInputEditText) findViewById(R.id.age);
        gender = (TextInputEditText) findViewById(R.id.gender);
        aadhar_no = (TextInputEditText) findViewById(R.id.aadhar_no);
        first_nm_nominee = (TextInputEditText) findViewById(R.id.first_nm_nominee);
        last_nm_nominee = (TextInputEditText) findViewById(R.id.last_nm_nominee);
        cal_premium_submit = (Button) findViewById(R.id.cal_premium_submit);
        send_otp_debit_submit = (Button) findViewById(R.id.send_otp_debit_btn);
        rg_customer_gender_type = (RadioGroup) findViewById(R.id.rg_trans_type);
        rg_trans_type_nominee = (RadioGroup) findViewById(R.id.rg_trans_type_nominee);
        area_nm_spinner = (Spinner) findViewById(R.id.state_spinner);
        nominee_relation_spinner = (Spinner) findViewById(R.id.nominee_relation_spinner);
        nominee_salutation_spinner = (Spinner) findViewById(R.id.nominee_salutation_spinner);
        customer_salutation_spinner = (Spinner) findViewById(R.id.customer_salutation_spinner);
        dob_nominee = (TextView) findViewById(R.id.dob_nominee);
        term_amount = (TextView) findViewById(R.id.term_amount);
        dob = (TextView) findViewById(R.id.dob);
        heading_txt = (TextView) findViewById(R.id.heading_txt);
        insured_amount_txt = (TextView) findViewById(R.id.insured_amount_txt);
        email_id = (EditText) findViewById(R.id.email_id);
        address_one = (EditText) findViewById(R.id.address_one);
        address_two = (EditText) findViewById(R.id.address_two);
        pin_edit = (EditText) findViewById(R.id.pin);
    }

    /* access modifiers changed from: private */
    public void updateLabel(int dayOfMonth, int monthOfYear, int year, String dob_type) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
        myCalendar.set(5, dayOfMonth);
        myCalendar.set(2, monthOfYear);
        myCalendar.set(1, year);
        try {
            age_calulate = Integer.parseInt(getAge(year, monthOfYear, dayOfMonth));
            if (age_calulate < 18 || age_calulate > 55) {
                Snackbar.make((View) snackbar_ll, (CharSequence) getResources().getString(R.string.valid_age_hospicash), Snackbar.LENGTH_LONG).show();
            } else if (dob_type.equalsIgnoreCase("nominee_dob")) {
                nominee_dob_selected = sdf.format(myCalendar.getTime());
                dob_nominee.setText(nominee_dob_selected);
                nomineeDOBConvert_str = getchangeDateFormat(nominee_dob_selected);
                nominee_age = age_calulate;
            } else {
                dob_selected = sdf.format(myCalendar.getTime());
                dob.setText(dob_selected);
                customerDOBConvert_str = getchangeDateFormat(dob_selected);
                customer_age = age_calulate;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void updatedecreaseDate_forcalculatePremiumAPi(int dayOfMonth, int monthOfYear, int year, String dob_type) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
        if (dob_type.equalsIgnoreCase("customer_dob")) {
            myCalendar.set(5, dayOfMonth - 1);
            myCalendar.set(2, monthOfYear);
            myCalendar.set(1, year);
        } else {
            myCalendar.set(5, dayOfMonth);
            myCalendar.set(2, monthOfYear);
            myCalendar.set(1, year);
        }
        try {
            age_calulate = Integer.parseInt(getAge(year, monthOfYear, dayOfMonth));
            if (age_calulate < 18 || age_calulate > 55) {
                Snackbar.make((View) snackbar_ll, (CharSequence) getResources().getString(R.string.valid_age_hospicash), Snackbar.LENGTH_LONG).show();
            } else if (dob_type.equalsIgnoreCase("nominee_dob")) {
                nominee_dob_selected = sdf.format(myCalendar.getTime());
                dob_nominee.setText(nominee_dob_selected);
                nomineeDOBConvert_str = getchangeDateFormat(nominee_dob_selected);
                nominee_age = age_calulate;
            } else {
                cus_dob_selected = sdf.format(myCalendar.getTime());
                customer_DOB_decrease_str = getchangeDateFormat(cus_dob_selected);
                customer_age = age_calulate;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getAge(int year, int month, int day) {
        Calendar dob2 = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        dob2.set(year, month, day);
        int age2 = today.get(1) - dob2.get(1);
        if (today.get(6) < dob2.get(6)) {
            age2--;
        }
        return new Integer(age2).toString();
    }

    public void getCustomerApi(String mobileno) {
        showLoader();
        JSONObject obj = new JSONObject();
        try {
            obj.put("number", mobileno);
            AndroidNetworking.post(GET_LOAD_WALLET_URL).setPriority(Priority.HIGH).addJSONObjectBody(obj).build().getAsJSONObject(new JSONObjectRequestListener() {
                public void onResponse(JSONObject response) {
                    try {
                        String key = new JSONObject(response.toString()).getString("hello");
                        PrintStream printStream = System.out;
                        StringBuilder sb = new StringBuilder();
                        sb.append(">>>>-----");
                        sb.append(key);
                        printStream.println(sb.toString());
                       getCustomerEncodedUrl(new String(Base64.decode(key, 0), Key.STRING_CHARSET_NAME));
                    } catch (JSONException e) {
                        hideLoader();
                    } catch (Exception e2) {
                        hideLoader();
                    }
                }

                public void onError(ANError anError) {
                }
            });
        } catch (JSONException e) {
        }
    }

    public void getCustomerEncodedUrl(String encodedURL) {
        AndroidNetworking.post(encodedURL).setPriority(Priority.HIGH)
                .addHeaders(HttpHeaders.AUTHORIZATION, this._token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

            public void onResponse(JSONObject response) {
               customer_LL.setVisibility(View.VISIBLE);
               customer_demographics_LL.setVisibility(View.GONE);
                try {
                    hideLoader();
                    if (response.getInt(NotificationCompat.CATEGORY_STATUS) == 0) {
                        customer_existing_nm = "";
                        customer_nm.setText(customer_existing_nm);
                        return;
                    }
                   customer_existing_nm = "";
                    customer_nm.setText(customer_existing_nm);
                } catch (JSONException e) {
                    hideLoader();
                }
            }

            public void onError(ANError anError) {
                hideLoader();
                anError.getResponse();
            }
        });
    }

    public void getDynamicEncodedURL(String link, final String type, final String gender2, String dob2) {
        showLoader();
        try {
            AndroidNetworking.get(link).setPriority(Priority.HIGH).build().getAsJSONObject(new JSONObjectRequestListener() {
                public void onResponse(JSONObject response) {
                    try {
                        String key = new JSONObject(response.toString()).getString("hello");
                        PrintStream printStream = System.out;
                        StringBuilder sb = new StringBuilder();
                        sb.append(">>>>-----");
                        sb.append(key);
                        printStream.println(sb.toString());
                        String encodedUrl = new String(Base64.decode(key, 0), Key.STRING_CHARSET_NAME);
                        if (type.equalsIgnoreCase("premium_calculate")) {
                            getPremiumCalculate_From_EncodedURL(encodedUrl, premium_total_amount, gender2,customer_DOB_decrease_str);
                        } else {
                            getSubmitLastData_From_EncodedURL(encodedUrl, lastData_json);
                        }
                    } catch (JSONException e) {
                        hideLoader();
                    } catch (Exception e2) {
                      hideLoader();
                    }
                }

                public void onError(ANError anError) {
                    hideLoader();
                }
            });
        } catch (Exception e) {
            hideLoader();
        }
    }

    public void getPremiumCalculate_From_EncodedURL(String encodedURL, String sumAssured, String gender2, String dob2) {
        try {
            JSONObject obj = new JSONObject();
            obj.putOpt("sumAssured", sumAssured);
            obj.putOpt("gender", gender2);
            obj.putOpt("dob", dob2);
            AndroidNetworking.post(encodedURL).setPriority(Priority.HIGH)
                    .addHeaders(HttpHeaders.AUTHORIZATION, _token)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                public void onResponse(JSONObject response) {
                    hideLoader();
                    try {
                        if (response.getInt(NotificationCompat.CATEGORY_STATUS) == 0) {
                            status_desc = response.optString("statusDesc");
                            premiumAmount = response.optString("premium");
                            jobID = response.optString("jobId");
                            TextView textView = insured_amount_txt;
                            StringBuilder sb = new StringBuilder();
                            sb.append("Insured Premium = INR ");
                            sb.append(premiumAmount);
                            textView.setText(sb.toString());
                            Util.showAlert(OneLakhINR_insuranceActivity.this, "Success !", status_desc);
                            return;
                        }
                        hideLoader();
                        Toast.makeText(OneLakhINR_insuranceActivity.this, "Premium Amount cannot be fetched !", 0).show();
                    } catch (Exception e) {
                        hideLoader();
                    }
                }

                public void onError(ANError anError) {
                    hideLoader();
                    Util.showAlert(OneLakhINR_insuranceActivity.this, "Error !", "Please try after sometime !");
                }
            });
        } catch (JSONException e) {
        }
    }

    public void getSubmitLastData_From_EncodedURL(String encodedURL, JSONObject last_json_data) {
        try {
            AndroidNetworking.post(encodedURL)
                    .setPriority(Priority.HIGH)
                    .addHeaders(HttpHeaders.AUTHORIZATION, _token)
                    .addJSONObjectBody(last_json_data)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                public void onResponse(JSONObject response) {
                    String str = "";
                    hideLoader();
                    try {
                        int status = response.getInt(NotificationCompat.CATEGORY_STATUS);
                        String str2 = str;
                        String status_desc = response.optString("statusDesc");
                        if (status == 0) {
                            String applicationID = response.optString("ApplicationId");
                            StringBuilder sb = new StringBuilder();
                            sb.append(status_desc);
                            sb.append(" \n Your Application id is : ");
                            sb.append(applicationID);
                            showAlert(OneLakhINR_insuranceActivity.this, "Success !", sb.toString());
                            return;
                        }
                        showAlert(OneLakhINR_insuranceActivity.this, "Info !", status_desc);
                    } catch (JSONException e) {
                        hideLoader();
                    } catch (Exception e2) {
                        hideLoader();
                    }
                }

                public void onError(ANError anError) {
                    hideLoader();
                    try {
                        Util.showAlert(OneLakhINR_insuranceActivity.this, "Error !", new JSONObject(anError.getErrorBody()).getString("statusDesc"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Util.showAlert(OneLakhINR_insuranceActivity.this, "Info !", "Please try after sometime !");
                    }
                }
            });
        } catch (Exception e) {
            hideLoader();
        }
    }

    public void showLoader() {
        try {
            if (this.pd != null) {
                this.handler.post(new Runnable() {
                    public void run() {
                        pd.setMessage("Loading...please wait !");
                        pd.setCancelable(false);
                        pd.show();
                    }
                });
            }
        } catch (Exception e) {
        }
    }

    public void hideLoader() {
        try {
            if (!isFinishing() && !isDestroyed() && pd != null && pd.isShowing()) {
                pd.dismiss();
                pd.cancel();
            }
        } catch (Exception e) {
        }
    }

    public String getchangeDateFormat(String selectedDate) {
        String formattedDate = null;
        try {
            if (VERSION.SDK_INT >= 26) {
                formattedDate = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH).format(LocalDate.parse(selectedDate, DateTimeFormatter.ofPattern("MM/dd/yyy", Locale.ENGLISH)));
            }
            System.out.println(formattedDate);
        } catch (Exception e) {
        }
        StringBuilder sb = new StringBuilder();
        sb.append(formattedDate);
        sb.append(getTime());
        return sb.toString();
    }

    public JSONObject getlast_data_json(String jobID2, String loanID, String customerFirstNm, String customerLastNm, String customerSalutation2, String customer_age2, String customer_gender, String emailID, String add1, String add2, String stateNm, String pinCode, String nomineeSalutation, String nomineeFirstNm, String nomineeLastNm, String nom_Gender, String nom_DOB, String cust_DOB, String nom_Relation, String premiumAmnt, String mobileNO, String nominee_Pcnt, String remark) {
        String str = customer_gender;
        String str2 = "";
        JSONObject obj = new JSONObject();
        try {
            obj.putOpt("TokenId", str2);
            try {
                obj.putOpt("JobId", jobID2);
                obj.putOpt("LoanId", str2);
                try {
                    obj.putOpt("BorrowerFirstName", customerFirstNm);
                } catch (JSONException e) {
                    String str3 = customerLastNm;
                    String str4 = customerSalutation2;
                    String str5 = customer_age2;
                    String str6 = emailID;
                    String str7 = add1;
                    String str8 = add2;
                    String str9 = stateNm;
                    String str10 = pinCode;
                    String str11 = nomineeSalutation;
                    String str12 = nomineeFirstNm;
                    String str13 = nomineeLastNm;
                    String str14 = remark;
                    return obj;
                }
            } catch (JSONException e2) {
                String str15 = customerFirstNm;
                String str32 = customerLastNm;
                String str42 = customerSalutation2;
                String str52 = customer_age2;
                String str62 = emailID;
                String str72 = add1;
                String str82 = add2;
                String str92 = stateNm;
                String str102 = pinCode;
                String str112 = nomineeSalutation;
                String str122 = nomineeFirstNm;
                String str132 = nomineeLastNm;
                String str142 = remark;
                return obj;
            }
            try {
                obj.putOpt("BorrowerSurName", customerLastNm);
                try {
                    obj.putOpt("BorrowerSalutationDesc", customerSalutation2);
                    try {
                        obj.putOpt("AgeAtCommencement", customer_age2);
                        obj.putOpt("ProposerGenderDesc", str);
                        obj.putOpt("GenderDesc", str);
                    } catch (JSONException e3) {
                        String str622 = emailID;
                        String str722 = add1;
                        String str822 = add2;
                        String str922 = stateNm;
                        String str1022 = pinCode;
                        String str1122 = nomineeSalutation;
                        String str1222 = nomineeFirstNm;
                        String str1322 = nomineeLastNm;
                        String str1422 = remark;
                        return obj;
                    }
                } catch (JSONException e4) {
                    String str522 = customer_age2;
                    String str6222 = emailID;
                    String str7222 = add1;
                    String str8222 = add2;
                    String str9222 = stateNm;
                    String str10222 = pinCode;
                    String str11222 = nomineeSalutation;
                    String str12222 = nomineeFirstNm;
                    String str13222 = nomineeLastNm;
                    String str14222 = remark;
                    return obj;
                }
            } catch (JSONException e5) {
                String str422 = customerSalutation2;
                String str5222 = customer_age2;
                String str62222 = emailID;
                String str72222 = add1;
                String str82222 = add2;
                String str92222 = stateNm;
                String str102222 = pinCode;
                String str112222 = nomineeSalutation;
                String str122222 = nomineeFirstNm;
                String str132222 = nomineeLastNm;
                String str142222 = remark;
                return obj;
            }
            try {
                obj.putOpt("EmailID", emailID);
                try {
                    obj.putOpt("AddressLine1", add1);
                    try {
                        obj.putOpt("AddressLine2", add2);
                        try {
                            obj.putOpt("StateName", stateNm);
                        } catch (JSONException e6) {
                            String str1022222 = pinCode;
                            String str1122222 = nomineeSalutation;
                            String str1222222 = nomineeFirstNm;
                            String str1322222 = nomineeLastNm;
                            String str1422222 = remark;
                            return obj;
                        }
                    } catch (JSONException e7) {
                        String str922222 = stateNm;
                        String str10222222 = pinCode;
                        String str11222222 = nomineeSalutation;
                        String str12222222 = nomineeFirstNm;
                        String str13222222 = nomineeLastNm;
                        String str14222222 = remark;
                        return obj;
                    }
                } catch (JSONException e8) {
                    String str822222 = add2;
                    String str9222222 = stateNm;
                    String str102222222 = pinCode;
                    String str112222222 = nomineeSalutation;
                    String str122222222 = nomineeFirstNm;
                    String str132222222 = nomineeLastNm;
                    String str142222222 = remark;
                    return obj;
                }
            } catch (JSONException e9) {
                String str722222 = add1;
                String str8222222 = add2;
                String str92222222 = stateNm;
                String str1022222222 = pinCode;
                String str1122222222 = nomineeSalutation;
                String str1222222222 = nomineeFirstNm;
                String str1322222222 = nomineeLastNm;
                String str1422222222 = remark;
                return obj;
            }
            try {
                obj.putOpt("PinCode", pinCode);
                try {
                    obj.putOpt("NomSalutation", nomineeSalutation);
                    try {
                        obj.putOpt("NomFirstName", nomineeFirstNm);
                        try {
                            obj.putOpt("NomSurname", nomineeLastNm);
                            obj.putOpt("NomGender", nom_Gender);
                            obj.putOpt("NomDOB", nom_DOB);
                            obj.putOpt("DateOfBirth", cust_DOB);
                            obj.putOpt("NomRelation", nom_Relation);
                            obj.putOpt("premiumAmount", premiumAmnt);
                            obj.putOpt("MobileNo", mobileNO);
                            obj.putOpt("NomPcnt", nominee_Pcnt);
                            try {
                                obj.putOpt("Remarks", remark);
                            } catch (JSONException e10) {
                            }
                        } catch (JSONException e11) {
                            String str14222222222 = remark;
                            return obj;
                        }
                    } catch (JSONException e12) {
                        String str13222222222 = nomineeLastNm;
                        String str142222222222 = remark;
                        return obj;
                    }
                } catch (JSONException e13) {
                    String str12222222222 = nomineeFirstNm;
                    String str132222222222 = nomineeLastNm;
                    String str1422222222222 = remark;
                    return obj;
                }
            } catch (JSONException e14) {
                String str11222222222 = nomineeSalutation;
                String str122222222222 = nomineeFirstNm;
                String str1322222222222 = nomineeLastNm;
                String str14222222222222 = remark;
                return obj;
            }
        } catch (JSONException e15) {
            String str16 = jobID2;
            String str152 = customerFirstNm;
            String str322 = customerLastNm;
            String str4222 = customerSalutation2;
            String str52222 = customer_age2;
            String str622222 = emailID;
            String str7222222 = add1;
            String str82222222 = add2;
            String str922222222 = stateNm;
            String str10222222222 = pinCode;
            String str112222222222 = nomineeSalutation;
            String str1222222222222 = nomineeFirstNm;
            String str13222222222222 = nomineeLastNm;
            String str142222222222222 = remark;
            return obj;
        }
        return obj;
    }

    public void showAlert(Context context, String title, String message) {
        Builder builder;
        try {
            if (VERSION.SDK_INT >= 21) {
                builder = new Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                builder = new Builder(context);
            }
            builder.setTitle((CharSequence) title).setMessage((CharSequence) message).setCancelable(false).setPositiveButton((CharSequence) "ok", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                    dialog.dismiss();
                }
            }).setNegativeButton((CharSequence) "Cancel", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.create().show();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public boolean validEmail(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public String getTime() {
        return new SimpleDateFormat("'T'HH:mm:ss.SSS'Z'").format(new Date(System.currentTimeMillis()));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        heading_txt.setText("INR 1 Lac Term");
        term_amount.setText("INR 1,00,000");
    }
    private void setFBAnalytics(String propertyKey, String propertyValue) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, getPackageName());
        //Logs an app event.
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        //Sets whether analytics collection is enabled for this app on this device.
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
        firebaseAnalytics.setSessionTimeoutDuration(500);
        firebaseAnalytics.setDefaultEventParameters(bundle);
        //Sets the user ID property.
        firebaseAnalytics.setUserId(getPackageName());
        //Sets a user property to a given value.
        firebaseAnalytics.setUserProperty(propertyKey, propertyValue);
    }
}
