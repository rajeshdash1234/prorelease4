package com.isuisudmt.bbps;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.isuisudmt.R;
import com.jsibbold.zoomage.ZoomageView;

public class ActivityPreviewBill extends AppCompatActivity {

    private ZoomageView demoView;
    String SampleBillURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_bill);

        setupToolbar();

        init();

        Glide.with(this)
                .load(SampleBillURL)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(demoView);

    }

    private void init() {
        SampleBillURL = getIntent().getStringExtra("SampleBillURL");
        demoView = findViewById(R.id.demoView);
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Sample Bill");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

    }

}