package com.isuisudmt.report.calender;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.isuisudmt.CustomThemes;
import com.isuisudmt.R;
import com.isuisudmt.report.calender.timesquare.CalendarCellDecorator;
import com.isuisudmt.report.calender.timesquare.CalendarPickerView;
import com.isuisudmt.report.calender.timesquare.DefaultDayViewAdapter;
import com.isuisudmt.report.util.ConstantsReport;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import static android.widget.Toast.LENGTH_SHORT;

public class CalenderActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    CalendarPickerView datePicker;
    ArrayList<Date> checkDateList = new ArrayList();
    Date dateThreeMonthBack, dateActive;

    TextView tvFromDate, tvToDate, tvSave;
    ImageView ivClose, ivReset;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);

        setContentView(R.layout.activity_calender);

        init();

        initCalender();

        datePicker.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                //String selectedDate = DateFormat.getDateInstance(DateFormat.FULL).format(date);
                Calendar calSelected = Calendar.getInstance();
                calSelected.setTime(date);

                Calendar calMaxSelectable = Calendar.getInstance();
                calMaxSelectable.setTime(date);
                calMaxSelectable.add(Calendar.DAY_OF_MONTH, 10);
                Date maxDateSelectable = calMaxSelectable.getTime();

                if (datePicker.getSelectedDates().size() == 1) {
                    checkDateList.clear();
                    checkDateList.add(date);


                    try {
                        String firstDate = "" + calSelected.get(Calendar.DAY_OF_MONTH)
                                + "-" + (calSelected.get(Calendar.MONTH) + 1)
                                + "-" + calSelected.get(Calendar.YEAR);

                        Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(firstDate);
                        ConstantsReport.selectFromDate = new SimpleDateFormat("yyyy-MM-dd").format(date2);
                        ConstantsReport.selectToDate = new SimpleDateFormat("yyyy-MM-dd").format(date2);

                        ConstantsReport.selectToolbarFromDate = new SimpleDateFormat("dd-MM-yyyy").format(date2);
                        ConstantsReport.selectToolbarToDate = new SimpleDateFormat("dd-MM-yyyy").format(date2);

                        tvFromDate.setText(new SimpleDateFormat("MMM dd").format(date2));
                        tvToDate.setText(new SimpleDateFormat("MMM dd").format(date2));

                        //Toast.makeText(CalenderActivity.this, "Select to date", LENGTH_SHORT).show();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    //Check if maxSelectableDate is after dateActive
                    if (maxDateSelectable.after(dateActive)) {
                        //if yes: set maxdate as today
                        datePicker.init(date, dateActive) //
                                .inMode(CalendarPickerView.SelectionMode.RANGE) //
                                .withSelectedDates(checkDateList);
                    } else {
                        //if no: set maxdate as 10days from selected from date
                        datePicker.init(date, maxDateSelectable) //
                                .inMode(CalendarPickerView.SelectionMode.RANGE) //
                                .withSelectedDates(checkDateList);
                    }


                } else {
                    if (datePicker.getSelectedDates().size() > 10) {
                        checkDateList.add(datePicker.getSelectedDates().get(9));

                        datePicker.setDecorators(Collections.<CalendarCellDecorator>emptyList());
                        datePicker.init(dateThreeMonthBack, dateActive) //
                                .inMode(CalendarPickerView.SelectionMode.RANGE) //
                                .withSelectedDates(checkDateList);

                        Toast.makeText(CalenderActivity.this, "More than 10 days can't be selected.", Toast.LENGTH_LONG).show();

                    } else {
                        //MonthView.setDividerColor(R.color.colorPrimary);
                        try {
                            String selectedDate = "" + calSelected.get(Calendar.DAY_OF_MONTH)
                                    + "-" + (calSelected.get(Calendar.MONTH) + 1)
                                    + "-" + calSelected.get(Calendar.YEAR);

                            Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(selectedDate);

                            tvToDate.setText(new SimpleDateFormat("MMM dd").format(date2));
                            ConstantsReport.selectToDate = new SimpleDateFormat("yyyy-MM-dd").format(date2);
                            ConstantsReport.selectToolbarToDate = new SimpleDateFormat("dd-MM-yyyy").format(date2);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onDateUnselected(Date date) {

            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        });

        ivReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvFromDate.setText("Start date");
                tvToDate.setText("End date");

                ConstantsReport.selectFromDate = "";
                ConstantsReport.selectToDate = "";
                ConstantsReport.selectToolbarFromDate = "";
                ConstantsReport.selectToolbarToDate = "";

                initCalender();
            }
        });

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConstantsReport.selectFromDate.equals(""))
                    Toast.makeText(CalenderActivity.this, "Please select Start date ", LENGTH_SHORT).show();
                else {
                    if (ConstantsReport.selectToDate.equals(""))
                        ConstantsReport.selectToDate = ConstantsReport.selectFromDate;

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("from_date", ConstantsReport.selectFromDate);
                    returnIntent.putExtra("to_date", ConstantsReport.selectToDate);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }

            }
        });

    }

    private void initCalender() {
        //Select From dates
        Calendar calenderThreeMonthBack = Calendar.getInstance();
        calenderThreeMonthBack.add(Calendar.DAY_OF_MONTH, -90);
        dateThreeMonthBack = calenderThreeMonthBack.getTime();

        //Select To dates
        Calendar calenderActive = Calendar.getInstance();
        calenderActive.add(Calendar.DAY_OF_MONTH, 1);
        dateActive = calenderActive.getTime();

        initButtonListeners(dateThreeMonthBack, dateActive);

    }

    private void init() {
        ConstantsReport.selectFromDate = "";
        ConstantsReport.selectToDate = "";
        ConstantsReport.selectToolbarFromDate = "";
        ConstantsReport.selectToolbarToDate = "";

        datePicker = findViewById(R.id.calendar);
        tvFromDate = findViewById(R.id.tv_from_date);
        tvToDate = findViewById(R.id.tv_to_date);
        tvSave = findViewById(R.id.tv_save);

        ivClose = findViewById(R.id.iv_calender_close);
        ivReset = findViewById(R.id.iv_calender_reset);
    }

    private void initButtonListeners(Date dateThreeMonthBack, Date dateActive) {
        datePicker.setCustomDayView(new DefaultDayViewAdapter());

        datePicker.setDecorators(Collections.<CalendarCellDecorator>emptyList());
        datePicker.init(dateThreeMonthBack, dateActive)
                .inMode(CalendarPickerView.SelectionMode.RANGE); //
        datePicker.scrollToDate(dateActive);

    }

}