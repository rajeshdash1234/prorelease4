package com.isuisudmt.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.Util;
import com.isuisudmt.report.model.ModelReportCommision2;
import com.paxsz.easylink.model.AppSelectResponse;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class AdapterReportCommision2 extends RecyclerView.Adapter<AdapterReportCommision2.ViewHolder> implements Filterable {

    private int lastSelectedPosition = -1;
    private Context context;
    private List<ModelReportCommision2> commisionReports = new ArrayList<>();
    private List<ModelReportCommision2> commisionReportsFiltered = new ArrayList<>();
    TextView fragAmount, fragTotal;

    public AdapterReportCommision2(Context context, List finoTransactionReports, TextView fragAmount, TextView fragTotal) {
        this.context = context;
        this.commisionReports = finoTransactionReports;
        this.commisionReportsFiltered = finoTransactionReports;
        this.fragTotal = fragTotal;
        this.fragAmount = fragAmount;}

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.commision_report_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ModelReportCommision2 current = commisionReportsFiltered.get(position);

        if (current.getAmountTransacted().toString().equals("null") || current.getAmountTransacted().toString().equals(null))
            holder.amount_transacted_recharge.setText(context.getResources().getString(R.string.toatlamountreport) + "N/A");
        else
            holder.amount_transacted_recharge.setText(context.getResources().getString(R.string.toatlamountreport) + current.getAmountTransacted());

        holder.updated_date.setText(new StringBuilder().append("Date: ").append(Util.getDateTime(String.valueOf(current.getUpdatedDate()))).toString());
        holder.status.setText(current.getStatus());
        holder.transaction_id_recharge.setText(new StringBuilder().append("Transaction Id: ").append(String.valueOf(current.getId())).toString());

        holder.mobile_number.setVisibility(View.GONE);
        holder.mobile_number.setVisibility(View.GONE);
        holder.operator_performed.setVisibility(View.GONE);
        holder.transaction_mode.setVisibility(View.GONE);


        holder.invoiceValue.setText(new StringBuilder().append("Type : ").append(current.getType()).toString());

        holder.pre_amount.setText("Previous Amount :₹ " + current.getPreviousAmount());
        holder.bal_amount.setText("Balanced Amount :₹ " + current.getBalanceAmount());

        if (current.getRelationalId().equals("null") || current.getRelationalId().equals(null))
            holder.relational_id.setText("Relational ID : " + current.getRelationalId());
        else
            holder.relational_id.setText("Relational ID : " + current.getRelationalId());


        if (current.getRelationalAmount().equals("null") || current.getRelationalAmount().equals(null))
            holder.relational_amount.setText("Relational Amount : N/A");
        else
            holder.relational_amount.setText("Relational Amount : " + current.getRelationalAmount());


        if (current.getRelationalOperation().equals("null") || current.getRelationalOperation().equals("") || current.getRelationalOperation().equals(null))
            holder.relational_op.setText("Relational Operation : N/A");
        else
            holder.relational_op.setText(new StringBuilder().append("Relational Operation : ").append(current.getRelationalOperation()).toString());

        if (current.getStatus().equals(AppSelectResponse.SUCCESS)) {
            holder.status.setTextColor(ContextCompat.getColor(this.context, R.color.color_report_green));
        } else {
            holder.status.setTextColor(ContextCompat.getColor(this.context, R.color.red));
        }
    }

    @Override
    public int getItemCount() {
        return commisionReportsFiltered.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView amount_transacted_recharge;
        public TextView mobile_number;
        public TextView operator_performed;
        public TextView status;
        public TextView transaction_id_recharge;
        public TextView transaction_mode;
        public TextView updated_date;
        public TextView invoiceValue, pre_amount, bal_amount, relational_op, relational_id, relational_amount;

        public ViewHolder(View itemView) {
            super(itemView);
            updated_date = itemView.findViewById(R.id.updated_date);
            status = itemView.findViewById(R.id.status);
            transaction_id_recharge = itemView.findViewById(R.id.transaction_id_recharge);
            amount_transacted_recharge = itemView.findViewById(R.id.amount_transacted);
            mobile_number = itemView.findViewById(R.id.mobile_number);
            operator_performed = itemView.findViewById(R.id.operation_performed);
            transaction_mode = itemView.findViewById(R.id.transactionMode);
            invoiceValue = itemView.findViewById(R.id.invoiceValue);
            pre_amount = itemView.findViewById(R.id.pre_amount);
            bal_amount = itemView.findViewById(R.id.bal_amount);
            relational_op = itemView.findViewById(R.id.relational_op);
            relational_id = itemView.findViewById(R.id.relational_id);
            relational_amount = itemView.findViewById(R.id.relational_amount);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    commisionReportsFiltered = commisionReports;
                } else {
                    List<ModelReportCommision2> filteredList = new ArrayList<>();
                    for (ModelReportCommision2 row : commisionReports) {

                        if (
                            row.getId().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }


                       /* if (String.valueOf(row.getId()).toLowerCase().contains(charString.toLowerCase()) || String.valueOf(row.getId()).contains(charSequence)) {
                            filteredList.add(row);
                        }*/
                    }
                    commisionReportsFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = commisionReportsFiltered;

                getTotalAmount();

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                commisionReportsFiltered = (ArrayList<ModelReportCommision2>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public double getTotalAmount() {
        double totalAmount = 0;
        DecimalFormat decim = new DecimalFormat("0.00");

        for (int i = 0; i < commisionReportsFiltered.size(); i++) {
            if (commisionReportsFiltered.get(i).getAmountTransacted().equals("N/A") || commisionReportsFiltered.get(i).getAmountTransacted().equals("null") ||
                    commisionReportsFiltered.get(i).getAmountTransacted().toString().equals(null)) {
                totalAmount += 0;
            } else {
                totalAmount += Double.parseDouble(String.valueOf(commisionReportsFiltered.get(i).getAmountTransacted()));
            }
        }

        fragAmount.setText(context.getResources().getString(R.string.toatlamountacitvity) + decim.format(totalAmount));
        fragTotal.setText("Entries: " + commisionReportsFiltered.size());

        return totalAmount;
    }

}
