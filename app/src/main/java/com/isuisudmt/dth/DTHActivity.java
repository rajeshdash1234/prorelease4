package com.isuisudmt.dth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.dmt.MyErrorMessage;
import com.isuisudmt.recharge.ReacargeStatusActivity;
import com.isuisudmt.utils.APIService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import static com.isuisudmt.BuildConfig.GET_AEPS_RECHARGE_URL;

public class DTHActivity extends AppCompatActivity {

    TextInputEditText dth_customerid, dth_operator, dth_amount;
    TextInputLayout layout_customerid, layout_operator, layout_amount;
    Button dth_button;
    String[] operatorDth = {"BIGTV DTH", "AIRTEL DTH", "TATASKY DTH", "SUN DIRECT DTH", "VIDEOCON DTH", "DISH TV DTH", "TATASKY B2B DTH"};
    String customeridString, amountString, operatorString, rechargetypeString, stdCodeString, pincodeString, accountNoString, latitudeString,
            circleCodeString, longitudeString;
    private APIService apiService;
    ProgressDialog pd;
    SessionManager session;
    String _token = "", _admin = "", userName = "";
    private FirebaseAnalytics firebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dth);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("DTH");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        dth_customerid = findViewById(R.id.customer_id);
        dth_operator = findViewById(R.id.operator);
        dth_amount = findViewById(R.id.amount);
        dth_button = findViewById(R.id.submit);

        layout_customerid = findViewById(R.id.layout_customerid);
        layout_operator = findViewById(R.id.layout_operator);
        layout_amount = findViewById(R.id.layout_amount);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rechargetypeString = "dth";
        stdCodeString = "";
        pincodeString = "";
        accountNoString = "";
        latitudeString = "";
        circleCodeString = "";
        longitudeString = "";

        dth_customerid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0) {
                    customeridString = null;
                } else {
                    customeridString = dth_customerid.getText().toString();
                }
            }
        });

        dth_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0) {
                    amountString = null;
                } else {
                    amountString = dth_amount.getText().toString();
                }
            }
        });

        dth_operator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFBAnalytics("DTH_OPERATOR", userName);

                View _view = getLayoutInflater().inflate(R.layout.custom_postpaid_operator, null);
                BottomSheetDialog dialog = new BottomSheetDialog(DTHActivity.this);
                dialog.setContentView(_view);

                ListView listView = _view.findViewById(R.id.listing);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(DTHActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, operatorDth);
                listView.setAdapter(adapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String itemValue = (String) listView.getItemAtPosition(position);
                        operatorString = itemValue;
                        dth_operator.setText(itemValue);
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        _token = user.get(SessionManager.KEY_TOKEN);
        _admin = user.get(SessionManager.KEY_ADMIN);
        userName = user.get(SessionManager.userName);

        dth_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (dth_customerid.getText().toString().isEmpty()) {
                    layout_customerid.setError("Enter valid customer id ");
                } else if (dth_operator.getText().toString().isEmpty()) {
                    layout_operator.setError("Select Operator");
                } else if (dth_amount.getText().toString().isEmpty()) {
                    layout_amount.setError("Enter amount");
                } else {
                    layout_customerid.setError(null);
                    layout_operator.setError(null);
                    layout_amount.setError(null);
                    setFBAnalytics("DTH_RECHARGE", userName);
                    rechargeDth();
                }
            }
        });
    }


    private void rechargeDth() {
        showLoader();
        final DthRequest dthRequest = new DthRequest(stdCodeString, pincodeString, amountString, customeridString, accountNoString, latitudeString, circleCodeString, operatorString, rechargetypeString, longitudeString);
        if (this.apiService == null) {
            this.apiService = new APIService();
        }

        AndroidNetworking.get(GET_AEPS_RECHARGE_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            //performEncodedRecharge(dthRequest,encodedUrl);
                            proceedRecharge(encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    public void proceedRecharge(String url) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("stdCode", stdCodeString);
            obj.put("amount", amountString);
            obj.put("mobileNumber", customeridString);
            obj.put("accountNo", accountNoString);
            obj.put("circleCode", circleCodeString);
            obj.put("operatorCode", operatorString);
            obj.put("rechargeType", rechargetypeString);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization", _token)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                System.out.println(obj.toString());
                                hideLoader();
                                if (obj.getString("status").equalsIgnoreCase("0")) {
                                    SuccessDialog(obj.getString("statusDesc"));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                ErrorDialog("RECHARGE FAILED! undefined");
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                            Gson gson = new Gson();
                            MyErrorMessage message = gson.fromJson(anError.getErrorBody(), MyErrorMessage.class);
                            ErrorDialog("RECHARGE FAILED! " + message.getMessage());
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void ErrorDialog(String msg) {

        clear();

        Intent intent = new Intent(getApplicationContext(), ReacargeStatusActivity.class);
        intent.putExtra("title", "DTH");
        intent.putExtra("status", "false");
        intent.putExtra("message", msg);
        startActivity(intent);
        /*AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(msg);
        builder1.setTitle("Info!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        clear();
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();*/
    }

    private void SuccessDialog(String msg) {

        clear();

        Intent intent = new Intent(getApplicationContext(), ReacargeStatusActivity.class);
        intent.putExtra("title", "DTH");
        intent.putExtra("status", "true");
        intent.putExtra("message", msg);
        startActivity(intent);

     /*   AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(msg);
        builder1.setTitle("Success!");
        builder1.setCancelable(false);
        builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                clear();
                dialog.cancel();
            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.show();*/
    }

    private void clear() {
        rechargetypeString = "dth";
        stdCodeString = "";
        pincodeString = "";
        accountNoString = "";
        latitudeString = "";
        circleCodeString = "";
        longitudeString = "";

        dth_customerid.setText("");
        dth_amount.setText("");
        dth_operator.setText("");

        layout_customerid.setError(null);
        layout_operator.setError(null);
        layout_amount.setError(null);
    }

    public void showLoader() {
        pd = new ProgressDialog(DTHActivity.this);
        pd.setMessage("loading");
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    public void hideLoader() {
        pd.hide();
    }

    private void setFBAnalytics(String propertyKey, String propertyValue) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, getPackageName());
        //Logs an app event.
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        //Sets whether analytics collection is enabled for this app on this device.
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
        firebaseAnalytics.setSessionTimeoutDuration(500);
        firebaseAnalytics.setDefaultEventParameters(bundle);
        //Sets the user ID property.
        firebaseAnalytics.setUserId(getPackageName());
        //Sets a user property to a given value.
        firebaseAnalytics.setUserProperty(propertyKey, propertyValue);
    }
}
