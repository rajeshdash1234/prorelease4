package com.isuisudmt;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddmoneyModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("statusDesc")
    @Expose
    private String statusDesc;
    @SerializedName("balance")
    @Expose
    private Integer balance;
    @SerializedName("fetchraisedRequests")
    @Expose
    private List<FetchraisedRequestModel> fetchraisedRequests = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public List<FetchraisedRequestModel> getFetchraisedRequests() {
        return fetchraisedRequests;
    }

    public void setFetchraisedRequests(List<FetchraisedRequestModel> fetchraisedRequests) {
        this.fetchraisedRequests = fetchraisedRequests;
    }
}
