package com.isuisudmt;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/*
 * For CustomThemes
 * @Author - Rashmi_Ranjan
 * */
public class CustomThemes extends View {

    Activity activity;
    Color color;
    SharedPreferences settings;
    int Theme_color_is;
    String Color;
    SharedPreferences.Editor editor;

    public CustomThemes(Activity context) {
        super(context);
        activity = context;
        Util.onActivityCreateSetTheme((AppCompatActivity) context);
        getColor(context);
        settings = context.getSharedPreferences("YOUR_PREF_NAME", 0);
        Theme_color_is = settings.getInt("SNOW_DENSITY", 0);


        if (Theme_color_is != 0) {
            editor = settings.edit();
            editor.putInt("SNOW_DENSITY", Theme_color_is);
            editor.commit();
            if (Theme_color_is == Util.THEME_YELLOW) {
                context.setTheme(R.style.YellowTheme);
                Util.putTheme(activity, "THEME_YELLOW");
            } else if (Theme_color_is == Util.THEME_BLUE) {
                context.setTheme(R.style.BlueTheme);
                Util.putTheme(activity, "THEME_BLUE");
            } else if (Theme_color_is == Util.THEME_DARK) {
                context.setTheme(R.style.DarkTheme);
                Util.putTheme(activity, "THEME_DARK");
            } else if (Theme_color_is == Util.THEME_RED) {
                context.setTheme(R.style.RedTheme);
                Util.putTheme(activity, "THEME_RED");
            } else if (Theme_color_is == Util.THEME_BROWN) {
                context.setTheme(R.style.BrownTheme);
                Util.putTheme(activity, "THEME_BROWN");
            } else if (Theme_color_is == Util.THEME_GREEN) {
                context.setTheme(R.style.GreenTheme);
                Util.putTheme(activity, "THEME_GREEN");
            } else if (Theme_color_is == Util.MediumSlateBlue) {
                context.setTheme(R.style.MediumSlateBlue);
                Util.putTheme(activity, "MediumSlateBlue");
            } else if (Theme_color_is == Util.Bluetiful) {
                context.setTheme(R.style.Bluetiful);
                Util.putTheme(activity, "Bluetiful");
            } else if (Theme_color_is == Util.VioletBlue) {
                context.setTheme(R.style.VioletBlue);
                Util.putTheme(activity, "VioletBlue");
            } else if (Theme_color_is == Util.PermanentGeraniumLake) {
                context.setTheme(R.style.PermanentGeraniumlake);
                Util.putTheme(activity, "PermanentGeraniumLake");
            } else {
                context.setTheme(R.style.AppTheme);
                Util.putTheme(activity, "DEFAULT");
            }

            // Util.putTheme(activity,snowDensity);
        }


        Color = settings.getString("Theme", "");
        Util.onActivityCreateSetTheme((AppCompatActivity) context);

        if (Theme_color_is == Util.THEME_YELLOW) {
            context.setTheme(R.style.YellowTheme);
        } else if (Theme_color_is == Util.THEME_BLUE) {
            context.setTheme(R.style.BlueTheme);
        } else if (Theme_color_is == Util.THEME_DARK) {
            context.setTheme(R.style.DarkTheme);
        } else if (Theme_color_is == Util.THEME_RED) {
            context.setTheme(R.style.RedTheme);
        } else if (Theme_color_is == Util.THEME_BROWN) {
            context.setTheme(R.style.BrownTheme);
        } else if (Theme_color_is == Util.THEME_GREEN) {
            context.setTheme(R.style.GreenTheme);
        } else if (Theme_color_is == Util.MediumSlateBlue) {
            context.setTheme(R.style.MediumSlateBlue);
        } else if (Theme_color_is == Util.Bluetiful) {
            context.setTheme(R.style.Bluetiful);
        } else if (Theme_color_is == Util.VioletBlue) {
            context.setTheme(R.style.VioletBlue);
        } else if (Theme_color_is == Util.PermanentGeraniumLake) {
            context.setTheme(R.style.PermanentGeraniumlake);
        } else {
            context.setTheme(R.style.AppTheme);
        }

    }


    public CustomThemes(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomThemes(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public CustomThemes(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    public static String getColor(Context context) {
        SharedPreferences settings = context.getSharedPreferences("YOUR_PREF_NAME", 0);
        String Color = settings.getString("Theme", "");

        return Color;
    }

}
