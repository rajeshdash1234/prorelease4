package com.isuisudmt.report;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.dmt.ReportFragmentRequest;
import com.isuisudmt.report.activity.ActivityReportDashboardNew;
import com.isuisudmt.report.adapter.AdapterReportCommision;
import com.isuisudmt.report.adapter.AdapterReportCommision2;
import com.isuisudmt.report.calender.CalenderActivity;
import com.isuisudmt.report.model.ModelReportCommision;
import com.isuisudmt.report.util.ConstantsReport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static android.view.View.GONE;
import static com.isuisudmt.report.util.ConstantsReport.CALENDER_PICKER_CODE;


public class FragmentReportCommision1 extends Fragment {

    TextView noData;

    private RecyclerView reportRecyclerView;
    AdapterReportCommision mAdapter;
    AdapterReportCommision2 mAdapter2;
    RecyclerView.LayoutManager layoutManager;
    AutoCompleteTextView transaction_spinner;
    ReportFragmentRequest reportFragmentRequest;
    ArrayList<ModelReportCommision> commisionReports = new ArrayList<>();
    //Button commision1,commision2;

    SessionManager session;
    String tokenStr="";
    ProgressBar progressV;
    Context context;
    String _userName;
    String fromdate="",todate="";
    Boolean calenderFlag = false;
    String reportType = "com1";

    TextView totalreport,amount;
    LinearLayout detailsLayout;

    DecimalFormat decim;

    public FragmentReportCommision1() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_commision,container,false);

        initView(rootView);

        reportType="com1";
        if(calenderFlag){
            //Do Nothing
        }else{
            callCalenderFunction();
        }

        return rootView;
    }

    private void initView(View rootView) {

        ((ActivityReportDashboardNew)getActivity()).getSupportActionBar().setTitle("Commission 1 Report");

        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        HashMap<String, String> user = session.getUserSession();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        _userName = user.get(SessionManager.userName);
        progressV = rootView.findViewById(R.id.progressV);
        noData = rootView.findViewById(R.id.noData);

        amount = rootView.findViewById ( R.id.amount );
        totalreport = rootView.findViewById ( R.id.totalreport );
        detailsLayout = rootView.findViewById ( R.id.detailsLayout );

        transaction_spinner = rootView.findViewById(R.id.transaction_spinner);
        reportRecyclerView = rootView.findViewById ( R.id.reportRecyclerView );
        reportRecyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        reportRecyclerView.setLayoutManager(layoutManager);

        decim = new DecimalFormat("0.00");

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        }
        else if(id == R.id.filterBar){
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print
                //Toast.makeText(getActivity(), "SearchOnQueryTextSubmit: " + query, Toast.LENGTH_SHORT).show();


                if( ! searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);

                if (commisionReports!=null && commisionReports.size() > 0) {
                    // filter recycler view when text is changed
                    mAdapter.getFilter().filter(s);
                    mAdapter.notifyDataSetChanged();

                    /*amount.setText(getResources().getString(R.string.toatlamountacitvity) + mAdapter.getTotalAmount());
                    totalreport.setText("Entries: " + mAdapter.getItems().size());*/

                }else{
                    if(calenderFlag == false)
                    Toast.makeText(getActivity(),getResources().getString(R.string.empty_date),Toast.LENGTH_LONG).show();
                }
                return false;
            }
        });

    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }

    private void callcommition1(String fromdate, String nextDate, String tokenStr) {

        showLoader();
        commisionReports.clear();

        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v67")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                            encryptedReport(fromdate,nextDate,tokenStr,encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                            hideLoader();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        System.out.println("Error");
                        hideLoader();

                    }
                });
    }

    public void encryptedReport(String fromDate, String toDate, String token,String strUrl){
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("fromDate",fromDate);
            jsonObject.put("toDate",toDate);
            jsonObject.put("transactionType","COMMISSION");

            AndroidNetworking.post(strUrl)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization",token)
                    .addJSONObjectBody(jsonObject)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                hideLoader();
                                commisionReports.clear();
                                JSONObject obj = new JSONObject(response.toString());
                                JSONArray jsonArray = obj.getJSONArray("BQReport");


                                if (jsonArray.length() == 0) {
                                    detailsLayout.setVisibility(View.GONE);
                                    noData.setVisibility(View.VISIBLE);
                                } else {
                                    for(int i =0 ; i<jsonArray.length();i++){
                                        noData.setVisibility(GONE);
                                        ModelReportCommision reportModel = new ModelReportCommision();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        reportModel.setId(jsonObject1.getString("Id"));
                                        reportModel.setAmountTransacted(jsonObject1.getDouble("amountTransacted"));
                                        reportModel.setPreviousAmount(jsonObject1.getDouble("previousAmount"));
                                        reportModel.setRelationalAmount(jsonObject1.getDouble("RelationalAmount"));
                                        reportModel.setBalanceAmount(jsonObject1.getDouble("balanceAmount"));

                                        reportModel.setgST(jsonObject1.getString("GST"));
                                        reportModel.settDS(jsonObject1.getString("TDS"));
                                        reportModel.setTaxable(jsonObject1.getString("Taxable"));
                                        reportModel.setInvoiceValue(jsonObject1.getString("InvoiceValue"));
                                        reportModel.setTransactionType(jsonObject1.getString("transactionType"));
                                        reportModel.setStatus(jsonObject1.getString("status"));
                                        reportModel.setCreditedUser(jsonObject1.getString("creditedUser"));
                                        reportModel.setDistributerName(jsonObject1.getString("distributerName"));
                                        reportModel.setMasterName(jsonObject1.getString("masterName"));
                                        reportModel.setTransactingUser(jsonObject1.getString("transactingUser"));
                                        reportModel.setRelationalOperation(jsonObject1.getString("RelationalOperation"));
                                        reportModel.setaPI(jsonObject1.getString("API"));
                                        reportModel.setCreatedDate(jsonObject1.getLong("createdDate"));
                                        reportModel.setUpdatedDate(jsonObject1.getLong("updatedDate"));


                                        commisionReports.add(reportModel);
                                    }
                                    mAdapter = new AdapterReportCommision(getActivity(), commisionReports, amount, totalreport);
                                    reportRecyclerView.setAdapter(mAdapter);
                                    hideLoader();
                                    Collections.reverse(commisionReports);

                                    totalreport.setText("Entries: " + commisionReports.size());
                                    double totalAmount = 0;
                                    for (int i = 0; i < commisionReports.size(); i++) {
                                        if (!commisionReports.get(i).getAmountTransacted().equals("null")) {
                                            totalAmount += Double.parseDouble(String.valueOf(commisionReports.get(i).getAmountTransacted()));
                                        }
                                    }
                                    detailsLayout.setVisibility(View.VISIBLE);
                                    amount.setText(getResources().getString(R.string.toatlamountacitvity) + decim.format(totalAmount));

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                noData.setVisibility(View.VISIBLE);

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.getErrorDetail();
                            transaction_spinner.setVisibility(GONE);
                            if(commisionReports.size()<=0){
                                noData.setVisibility(View.VISIBLE);
                            }else{
                                noData.setVisibility(GONE);
                            }
                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void showLoader() {
        progressV.setVisibility(View.VISIBLE);
    }

    private void hideLoader() {
        progressV.setVisibility(GONE);


    }


    private void callCalenderFunction() {
        Intent intent = new Intent(getActivity(), CalenderActivity.class);
        startActivityForResult(intent, CALENDER_PICKER_CODE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CALENDER_PICKER_CODE) {
            if(resultCode == Activity.RESULT_OK){
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectToolbarFromDate +" to " + ConstantsReport.selectToolbarToDate);
                calenderFlag = true;

                //Make API call
                callcommition1(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate), tokenStr);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle("");
                calenderFlag = false;
                //Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
