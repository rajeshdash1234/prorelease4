package com.isuisudmt.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.PrepaidBBPS;
import com.isuisudmt.R;
import com.isuisudmt.Util;
import com.paxsz.easylink.model.AppSelectResponse;

import java.util.ArrayList;
import java.util.List;

public class AdapterPrepaid extends RecyclerView.Adapter<AdapterPrepaid.ViewHolder> implements Filterable {

    private Context context;
    public List<PrepaidBBPS> pojoReportBBPS;
    public List<PrepaidBBPS> pojoReportBBPSFilter;

    TextView fragAmount, fragTotal;

    public AdapterPrepaid(Context context2, List pojoReportBBPS, TextView fragAmount, TextView fragTotal) {
        this.context = context2;
        this.pojoReportBBPS = pojoReportBBPS;
        this.pojoReportBBPSFilter = pojoReportBBPS;
        this.fragTotal = fragTotal;
        this.fragAmount = fragAmount;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_report_prepaid, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PrepaidBBPS current = pojoReportBBPSFilter.get(position);

        if (current.getAmountTransacted().toString().equals("null") || current.getAmountTransacted().toString().equals(null))
            holder.amount_transacted_recharge.setText(context.getResources().getString(R.string.toatlamountreport) + "N/A");
        else
            holder.amount_transacted_recharge.setText(context.getResources().getString(R.string.toatlamountreport) + current.getAmountTransacted());

        holder.updated_date.setText(new StringBuilder().append("Date: ").append(Util.getDateTime(String.valueOf(current.getUpdatedDate()))).toString());
        holder.status.setText(current.getStatus());
        holder.transaction_id_recharge.setText(new StringBuilder().append("Transaction Id: ").append(String.valueOf(current.getId())).toString());
        if (current.apiTid.equalsIgnoreCase("null")) {
            holder.trackingId.setText(new StringBuilder().append("User Tracking Id: ").append("N/A"));
        } else {
            holder.trackingId.setText(new StringBuilder().append("User Tracking Id: ").append(String.valueOf(current.getApiTid())).toString());

        }

        holder.mobile_number.setText(new StringBuilder().append("Mobile No:").append(current.getMobileNumber()).toString());


        if (current.getStatus().equals(AppSelectResponse.SUCCESS)) {
            holder.status.setTextColor(ContextCompat.getColor(context, R.color.color_report_green));
        } else if (current.getStatus().equalsIgnoreCase("INITIATED")) {
            holder.status.setTextColor(ContextCompat.getColor(context, R.color.orange));
        } else {
            holder.status.setTextColor(ContextCompat.getColor(context, R.color.red));
        }

        holder.operator.setText(new StringBuilder().append("Operator Name: ").append(current.getOperatorDescription()));

    }

    @Override
    public int getItemCount() {
        return pojoReportBBPSFilter.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            public FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    pojoReportBBPSFilter = pojoReportBBPS;
                } else {
                    List<PrepaidBBPS> filteredList = new ArrayList<>();
                    for (PrepaidBBPS row : pojoReportBBPS) {
                        if (row.getId().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getMobileNumber().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getOperatorDescription().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getStatus().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getApiTid().toLowerCase().contains(charString.toLowerCase())
                        ) {
                            filteredList.add(row);
                        }

                    }
                    pojoReportBBPSFilter = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = pojoReportBBPSFilter;
                //Log.d(TAG, "performFiltering: " + pojoReportBBPSFilter);
                getTotalAmount();

                return filterResults;
            }

            @Override
            public void publishResults(CharSequence charSequence, FilterResults filterResults) {
                pojoReportBBPSFilter = (List<PrepaidBBPS>)  filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView amount_transacted_recharge;
        public TextView trackingId;
        public TextView mobile_number;
        public TextView status;
        public TextView transaction_id_recharge;
        public TextView transaction_mode;
        public TextView updated_date;
        public TextView operator;

        public ViewHolder(View itemView) {
            super(itemView);

            updated_date = itemView.findViewById(R.id.updated_date);
            status = itemView.findViewById(R.id.status);
            transaction_id_recharge = itemView.findViewById(R.id.transaction_id_recharge);
            amount_transacted_recharge = itemView.findViewById(R.id.amount_transacted);
            mobile_number = itemView.findViewById(R.id.mobile_number);
            transaction_mode = itemView.findViewById(R.id.transactionMode);
            operator = itemView.findViewById(R.id.operator);
            trackingId = itemView.findViewById(R.id.tracking_id);
        }
    }

    public double getTotalAmount() {
        double totalAmount = 0;
        for (int i = 0; i < pojoReportBBPSFilter.size(); i++) {
            if (pojoReportBBPSFilter.get(i).getAmountTransacted().equals("N/A") || pojoReportBBPSFilter.get(i).getAmountTransacted().equals("null") ||
                    pojoReportBBPSFilter.get(i).getAmountTransacted().toString().equals(null)) {
                totalAmount += 0;
            } else {
                totalAmount += Double.parseDouble(String.valueOf(pojoReportBBPSFilter.get(i).getAmountTransacted()));
            }
        }

        fragAmount.setText(context.getResources().getString(R.string.toatlamountacitvity) + totalAmount);
        fragTotal.setText("Entries: " + pojoReportBBPSFilter.size());

        return totalAmount;
    }
}
