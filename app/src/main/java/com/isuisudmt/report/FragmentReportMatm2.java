package com.isuisudmt.report;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.tabs.TabLayout;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.matm.MicroReportContract;
import com.isuisudmt.matm.MicroReportModel;
import com.isuisudmt.matm.MicroReportPresenter;
import com.isuisudmt.matm.RefreshModel;
import com.isuisudmt.report.activity.ActivityReportDashboardNew;
import com.isuisudmt.report.adapter.AdapterReportMATM;
import com.isuisudmt.report.calender.CalenderActivity;
import com.isuisudmt.report.util.ConstantsReport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static com.isuisudmt.report.util.ConstantsReport.CALENDER_PICKER_CODE;

public class FragmentReportMatm2 extends Fragment implements MicroReportContract.View {

    private static RecyclerView reportRecyclerView;
    private static MicroReportPresenter mActionsListener;
    static ArrayList<MicroReportModel> reportResponseArrayList = new ArrayList<MicroReportModel>();

    TextView totalreport, amount;
    LinearLayout detailsLayout;


    private static AdapterReportMATM adapterReportMATM;
    TextView noData;
    static String clientId = "";
    SessionManager session;
    static String tokenStr = "";
    static String trans_type = "MATM2";
    ProgressBar progressBar;
    static Context context;

    public String matam_type = "MATM_FUND_TRANSFER2";

    SharedPreferences sp;
    public static final String ISU_PREF = "isuPref";
    public static final String USER_NAME = "userNameKey";
    JSONArray jArrayStatus, jArrayTransactionType;
    JSONArray jArrayOpsPerformed;//All or


    String[] arr_operation_performed = {"MATM2_CASH_WITHDRAWAL", "MATM2_BALANCE_ENQUIRY"};
    String selectedStatus = "All";

    private TabLayout tablayout;
    String SELECTED_TYPE = "all";
    String mATM2ReportResponse = "";
    Boolean calenderFlag = false;
    ProgressDialog dialog;

    public FragmentReportMatm2() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_matm, container, false);
        pref = getActivity().getSharedPreferences("DEFUALT", 0);
        editor = pref.edit();

        AdapterReportMATM.transType = "";

        initView(rootView);

        callTablistener();

        if (calenderFlag) {
            ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectFromDate + " to " + ConstantsReport.selectToDate);
            getReport(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate), tokenStr, trans_type, matam_type);
        } else {
            callCalenderFunction();
        }

        return rootView;
    }

    private void callTablistener() {
        tablayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                getActivity().invalidateOptionsMenu();

                if (tab.getPosition() == 0) {
                    SELECTED_TYPE = "all";
                    if (calenderFlag) {
                        if (!mATM2ReportResponse.equals("")) {
                            filterResponse(mATM2ReportResponse, "all");
                        }
                    } else {
                        callCalenderFunction();
                    }

                } else if (tab.getPosition() == 1) {
                    SELECTED_TYPE = "MATM2_CASH_WITHDRAWAL";
                    if (calenderFlag) {
                        if (!mATM2ReportResponse.equals("")) {
                            filterResponse(mATM2ReportResponse, SELECTED_TYPE);
                        }
                    } else {
                        callCalenderFunction();
                    }

                } else if (tab.getPosition() == 2) {
                    SELECTED_TYPE = "MATM2_BALANCE_ENQUIRY";
                    if (calenderFlag) {
                        if (!mATM2ReportResponse.equals("")) {
                            filterResponse(mATM2ReportResponse, SELECTED_TYPE);
                        }
                    } else {
                        callCalenderFunction();
                    }

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        } else if (id == R.id.filterBar) {
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }

    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                try {
                    if (reportResponseArrayList != null && reportResponseArrayList.size() > 0) {
                        adapterReportMATM.getFilter().filter(s);
                        adapterReportMATM.notifyDataSetChanged();
                    } else {
                        if (calenderFlag == false)
                            Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {

                }
                return false;
            }
        });

    }


    private void initView(View rootView) {

        sp = getActivity().getSharedPreferences(ISU_PREF, Context.MODE_PRIVATE);

        ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setTitle("mATM 2 Report");
        ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle("");

        jArrayTransactionType = new JSONArray();
        jArrayTransactionType.put("MATM2");

        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);

        dialog = new ProgressDialog(getActivity());

        tablayout = rootView.findViewById(R.id.tablayout);
        progressBar = rootView.findViewById(R.id.progressBar);
        noData = rootView.findViewById(R.id.noData);
        amount = rootView.findViewById(R.id.amount);
        totalreport = rootView.findViewById(R.id.totalreport);
        detailsLayout = rootView.findViewById(R.id.detailsLayout);
        reportRecyclerView = rootView.findViewById(R.id.reportRecyclerView);

        reportRecyclerView.setHasFixedSize(true);
        reportRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mActionsListener = new MicroReportPresenter(this);

        jArrayOpsPerformed = new JSONArray();
        for (int i = 0; i < arr_operation_performed.length; i++) {
            jArrayOpsPerformed.put(arr_operation_performed[i]);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void reportsReady(ArrayList<MicroReportModel> reportModelArrayList, String totalAmount) {
        reportResponseArrayList = reportModelArrayList;
        amount.setText("Amt Tnx: ₹" + totalAmount);
    }

    @Override
    public void refreshDone(RefreshModel refreshModel) {

        try {
            editor.putBoolean("INTENT", true);
            editor.commit();

            boolean installed = appInstalledOrNot("com.matm.matmservice");
            if (installed) {
                Intent intent = new Intent(Intent.ACTION_DATE_CHANGED);
                PackageManager manager = getActivity().getPackageManager();
                intent = manager.getLaunchIntentForPackage("com.matm.matmservice");
                intent.putExtra("RequestData", refreshModel.getEncData());
                intent.putExtra("HeaderData", refreshModel.getAuthentication());
                intent.putExtra("ReturnTime", 5);
                intent.putExtra("IS_PAIR_DEVICE", false);
                intent.putExtra("Flag", "transaction");
                intent.putExtra("TransactionType", "refresh");
                intent.putExtra("client_id", clientId);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                startActivityForResult(intent, 1);
            } else {
                showAlert(getActivity());
                System.out.println("App is not installed on your phone");
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void showReports() {

        if (reportResponseArrayList != null && reportResponseArrayList.size() > 0) {

            reportRecyclerView.setVisibility(View.VISIBLE);
            detailsLayout.setVisibility(View.VISIBLE);
            noData.setVisibility(View.GONE);

            adapterReportMATM = new AdapterReportMATM(getActivity(), reportResponseArrayList, trans_type, new AdapterReportMATM.IMethodCaller() {
                @Override
                public void refreshMethod(MicroReportModel microReportModel) {
                    boolean installed = appInstalledOrNot("com.matm.matmservice");
                    if (installed) {
                        clientId = String.valueOf(microReportModel.getId());
                        mActionsListener.refreshReports(tokenStr, String.valueOf(microReportModel.getAmountTransacted()), "statusEnquiry", "mobile", String.valueOf(microReportModel.getId()));

                    } else {
                        showAlert(getActivity());

                        System.out.println("App is not installed on your phone");
                    }
                }
            }, amount, totalreport);
            reportRecyclerView.setAdapter(adapterReportMATM);
            adapterReportMATM.notifyDataSetChanged();
            totalreport.setText("Entries: " + reportResponseArrayList.size());
        } else {
            reportRecyclerView.setVisibility(View.GONE);
            detailsLayout.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
            totalreport.setText("Entries: " + 0);

        }

    }

    private static boolean appInstalledOrNot(String uri) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }

    @Override
    public void showLoader() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void emptyDates() {
        Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();

    }

    @Override
    public void checkAmount(String status) {
        if (status != null && status.matches("1")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.amountrefersh), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkTransactionMode(String status) {
        if (status != null && status.matches("1")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.transaction_mode_refersh), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkTransactionType(String status) {
        if (status != null && status.matches("1")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.transaction_type_refersh), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkClientId(String status) {

    }

    @Override
    public void emptyRefreshData(String status) {

    }

    public static void showAlert(Context context) {
        AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new AlertDialog.Builder(context);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Please download the MATM service app from the playstore.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton("Download Now", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        redirectToPlayStore();
                    }
                })
                .setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public static void redirectToPlayStore() {
        Uri uri = Uri.parse("market://details?id=com.matm.matmservice");
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=com.matm.matmservice")));
        }
    }


    private void getReport(final String fromDate, final String toDate, final String token, final String transactionType, final String matmtype) {
        String URL = "https://us-central1-creditapp-29bf2.cloudfunctions.net/report_matm_2";

        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();
        try {

            obj.put("startDate", fromDate);
            obj.put("endDate", toDate);
            obj.put("userName", sp.getString(USER_NAME, ""));
            obj.put("transaction_type", jArrayTransactionType);
            obj.put("operationPerformed", jArrayOpsPerformed);
            obj.put("status", selectedStatus);

        } catch (Exception e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(URL)
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .addHeaders("Authorization", tokenStr)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.cancel();

                        mATM2ReportResponse = response.toString();
                        filterResponse(mATM2ReportResponse, SELECTED_TYPE);

                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.cancel();

                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(anError.getErrorBody().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Toast.makeText(getActivity(), obj.getString("statusDescription").toString(), Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                });

    }

    private void filterResponse(String aePS2ReportResponse, String Type) {
        try {
            reportResponseArrayList.clear();
            reportRecyclerView.setVisibility(View.VISIBLE);
            noData.setVisibility(View.GONE);

            JSONObject obj = new JSONObject(aePS2ReportResponse);
            String status = obj.optString("status");
            String message = obj.optString("message");
            //String results = obj.getString("results");
            String length = obj.optString("length");

            if (length.equals("0")) {
                detailsLayout.setVisibility(View.GONE);
                noData.setVisibility(View.VISIBLE);
                reportRecyclerView.setVisibility(View.GONE);
            } else {
                if (status.equals("200")) {

                    String BQReport = obj.getString("BQReport");
                    JSONArray jsonArray = new JSONArray(BQReport);

                    if (Type.equals("all")) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            MicroReportModel reportModel = new MicroReportModel();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            reportModel.setId(jsonObject1.getString("Id"));

                            String previousAmount = jsonObject1.getString("previousAmount");
                            if (!previousAmount.equalsIgnoreCase("null")) {
                                reportModel.setPreviousAmount(previousAmount);
                            } else {
                                reportModel.setPreviousAmount("N/A");
                            }

                            String balanceAmount = jsonObject1.getString("balanceAmount");
                            if (!balanceAmount.equalsIgnoreCase("null")) {
                                reportModel.setBalanceAmount(balanceAmount);
                            } else {
                                reportModel.setBalanceAmount("N/A");
                            }

                            String amountTransactedStr = jsonObject1.getString("amountTransacted");
                            if (!amountTransactedStr.equalsIgnoreCase("null")) {
                                reportModel.setAmountTransacted(amountTransactedStr);
                            } else {
                                reportModel.setAmountTransacted("N/A");
                            }


                            // reportModel.setAmountTransacted(jsonObject1.getString("amountTransacted"));
                            reportModel.setApiTId(jsonObject1.getString("apiTid"));
                            reportModel.setStatus(jsonObject1.getString("status"));
                            reportModel.setTransactionMode(jsonObject1.getString("operationPerformed"));
                            reportModel.setOperationPerformed(jsonObject1.getString("operationPerformed"));
                            reportModel.setUserName(jsonObject1.getString("userName"));
                            reportModel.setDistributerName(jsonObject1.getString("distributerName"));
                            reportModel.setMasterName(jsonObject1.getString("masterName"));
                            reportModel.setUserTrackId(jsonObject1.getString("UserTrackId"));
                            reportModel.setCardDetail(jsonObject1.getString("cardDetails"));
                            reportModel.setCreatedDate(String.valueOf(jsonObject1.getLong("createdDate")));
                            reportModel.setUpdatedDate(String.valueOf(jsonObject1.getLong("updatedDate")));
                            // reportModel.setReferenceNo(jsonObject1.getString("referenceNo"));

                            reportResponseArrayList.add(reportModel);

                        }
                    } else {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            MicroReportModel reportModel = new MicroReportModel();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            if (jsonObject1.getString("operationPerformed").equals(Type)) {
                                reportModel.setId(jsonObject1.getString("Id"));

                                String previousAmount = jsonObject1.getString("previousAmount");
                                if (!previousAmount.equalsIgnoreCase("null")) {
                                    reportModel.setPreviousAmount(previousAmount);
                                } else {
                                    reportModel.setPreviousAmount("N/A");
                                }

                                String balanceAmount = jsonObject1.getString("balanceAmount");
                                if (!balanceAmount.equalsIgnoreCase("null")) {
                                    reportModel.setBalanceAmount(balanceAmount);
                                } else {
                                    reportModel.setBalanceAmount("N/A");
                                }

                                String amountTransactedStr = jsonObject1.getString("amountTransacted");
                                if (!amountTransactedStr.equalsIgnoreCase("null")) {
                                    reportModel.setAmountTransacted(amountTransactedStr);
                                } else {
                                    reportModel.setAmountTransacted("N/A");
                                }


                                // reportModel.setAmountTransacted(jsonObject1.getString("amountTransacted"));
                                reportModel.setApiTId(jsonObject1.getString("apiTid"));
                                reportModel.setStatus(jsonObject1.getString("status"));
                                reportModel.setTransactionMode(jsonObject1.getString("operationPerformed"));
                                reportModel.setOperationPerformed(jsonObject1.getString("operationPerformed"));
                                reportModel.setUserName(jsonObject1.getString("userName"));
                                reportModel.setDistributerName(jsonObject1.getString("distributerName"));
                                reportModel.setMasterName(jsonObject1.getString("masterName"));
                                reportModel.setUserTrackId(jsonObject1.getString("UserTrackId"));
                                reportModel.setCardDetail(jsonObject1.getString("cardDetails"));
                                reportModel.setCreatedDate(String.valueOf(jsonObject1.getLong("createdDate")));
                                reportModel.setUpdatedDate(String.valueOf(jsonObject1.getLong("updatedDate")));
                                // reportModel.setReferenceNo(jsonObject1.getString("referenceNo"));

                                reportResponseArrayList.add(reportModel);

                            }
                        }
                    }


                    double totalAmount = 0;
                    for (int i = 0; i < reportResponseArrayList.size(); i++) {
                        if (reportResponseArrayList.get(i).getAmountTransacted().equalsIgnoreCase("N/A") || reportResponseArrayList.get(i).getAmountTransacted().equalsIgnoreCase("null")) {
                            totalAmount += 0;
                        } else {
                            totalAmount += Double.parseDouble(String.valueOf(reportResponseArrayList.get(i).getAmountTransacted()));
                        }
                    }

                    detailsLayout.setVisibility(View.VISIBLE);
                    amount.setText(getResources().getString(R.string.toatlamountacitvity) + totalAmount);

                    if (reportResponseArrayList != null && reportResponseArrayList.size() > 0) {

                        reportRecyclerView.setVisibility(View.VISIBLE);
                        noData.setVisibility(View.GONE);

                        adapterReportMATM = new AdapterReportMATM(getActivity(), reportResponseArrayList, trans_type, new AdapterReportMATM.IMethodCaller() {
                            @Override
                            public void refreshMethod(MicroReportModel microReportModel) {
                                boolean installed = appInstalledOrNot("com.matm.matmservice");
                                if (installed) {
                                    clientId = String.valueOf(microReportModel.getId());
                                    mActionsListener.refreshReports(tokenStr, String.valueOf(microReportModel.getAmountTransacted()), "statusEnquiry", "mobile", String.valueOf(microReportModel.getId()));

                                } else {
                                    showAlert(getActivity());

                                    System.out.println("App is not installed on your phone");
                                }
                            }
                        }, amount, totalreport);

                        reportRecyclerView.setAdapter(adapterReportMATM);
                        adapterReportMATM.notifyDataSetChanged();
                        totalreport.setText("Entries: " + reportResponseArrayList.size());

                        Collections.reverse(reportResponseArrayList);
                    } else {
                        reportRecyclerView.setVisibility(View.GONE);
                        noData.setVisibility(View.VISIBLE);
                        detailsLayout.setVisibility(View.GONE);
                        totalreport.setText("Entries: " + 0);

                    }


                    dialog.cancel();


                } else {
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                    dialog.cancel();
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
            dialog.cancel();
        }
    }

    private void callCalenderFunction() {
        getActivity().invalidateOptionsMenu();
        Intent intent = new Intent(getActivity(), CalenderActivity.class);
        startActivityForResult(intent, CALENDER_PICKER_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CALENDER_PICKER_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectToolbarFromDate +" to " + ConstantsReport.selectToolbarToDate);
                calenderFlag = true;

                //Make API call
                getReport(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate), tokenStr, trans_type, matam_type);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle("");
                calenderFlag = false;
                //Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}