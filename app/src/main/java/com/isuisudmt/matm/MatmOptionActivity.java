package com.isuisudmt.matm;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.isuisudmt.Constants;
import com.isuisudmt.CustomThemes;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.bluetooth.SharePreferenceClass;
import com.matm.matmsdk.Error.ErrorMatm1Activity;
import com.matm.matmsdk.MPOS.PosServiceActivity;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.matm1.Matm1TransactionStatusActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.isuisudmt.bbps.utils.Const.isMATM1enabled;
import static com.isuisudmt.bbps.utils.Const.isMATM2enabled;

public class MatmOptionActivity extends AppCompatActivity implements View.OnClickListener, MicroAtmContract.View,GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    String _token = "";
    SessionManager sessionManager;
    String _admin = "";
    String _userName;
    SharePreferenceClass sharePreferenceClass;
    private ProgressDialog dialog;
    String strMatmType = "Matm2";
    String strTransType = "Cash Withdrawal";
    String transactionAmount = "";
    RadioGroup rg_matm_type;
    RadioButton rb_matm1, rb_matm2;
    Button submitMatm;
    boolean matm_transaction_intent = false;
    String encData;
    String authentication;

    public MicroAtmPresenter microAtmPresenter;
    MicroAtmRequestModel microAtmRequestModel;
    String responseData = "";

    private static final int REQUEST_CA_PERMISSIONS = 931;
    Double latitude, longitude;
    private Location mylocation;
    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private Boolean location_flag = false;
    String lat = "0.0";
    String lng = "0.0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);
        setContentView(R.layout.activity_matm_option);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Micro ATM");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        sessionManager = new SessionManager(MatmOptionActivity.this);
        microAtmPresenter = new MicroAtmPresenter(this);

        setUpGClient();
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                checkPermission();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        HashMap<String, String> user = sessionManager.getUserDetails();
        _token = user.get(SessionManager.KEY_TOKEN);
        _admin = user.get(SessionManager.KEY_ADMIN);

        HashMap<String, String> user_ = sessionManager.getUserSession();
        _userName = user_.get(sessionManager.userName);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        sharePreferenceClass = new SharePreferenceClass(MatmOptionActivity.this);

        transactionAmount = getIntent().getStringExtra("AMOUNT");
        strTransType = getIntent().getStringExtra("AEPSTRANS_TYPE");

        rg_matm_type = findViewById(R.id.rg_matm_type);
        rb_matm1 = findViewById(R.id.rb_matm1);
        rb_matm2 = findViewById(R.id.rb_matm2);
        submitMatm = findViewById(R.id.submitMatm);
        submitMatm.setEnabled(true);

        if (Constants.user_feature_array != null) {
            //Check Feature
            if (isMATM1enabled == false) {
                rb_matm1.setBackgroundResource(R.color.service_unavailable_col);
                rb_matm1.setEnabled(false);
            }
            if (isMATM2enabled == false) {
                rb_matm2.setBackgroundResource(R.color.service_unavailable_col);
                rb_matm2.setEnabled(false);
                rb_matm2.setChecked(false);
            }
        }

        rg_matm_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_matm1:
                        strMatmType = "MATM1";
                        submitMatm.setEnabled(true);

                        break;
                    case R.id.rb_matm2:
                        strMatmType = "MATM2";
                        submitMatm.setEnabled(true);
                        break;
                }
            }
        });
        submitMatm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                strMatmType = "MATM2";
//                showLoader();
//                refreshToken(strMatmType);
                submitMatm.setEnabled(false);
                if (rb_matm1.isChecked()) {
                    showLoader();
                    refreshToken(strMatmType);
                } else if (rb_matm2.isChecked()) {
                    showLoader();
                    refreshToken(strMatmType);
                }
            }
        });
    }

    public String getShortState(String state) {
        try {
            if (state.equalsIgnoreCase("Andhra Pradesh")) {
                return "AP";
            } else if (state.equalsIgnoreCase("Arunachal Pradesh")) {
                return "AR";
            } else if (state.equalsIgnoreCase("Assam")) {
                return "AS";
            } else if (state.equalsIgnoreCase("Bihar")) {
                return "BR";
            } else if (state.equalsIgnoreCase("Chandigarh")) {
                return "CH";
            } else if (state.equalsIgnoreCase("Chhattisgarh")) {
                return "CG";
            } else if (state.equalsIgnoreCase("Dadra and Nagar Haveli")) {
                return "DN";
            } else if (state.equalsIgnoreCase("Daman & Diu")) {
                return "DD";
            } else if (state.equalsIgnoreCase("Delhi")) {
                return "DL";
            } else if (state.equalsIgnoreCase("Goa")) {
                return "GA";
            } else if (state.equalsIgnoreCase("Gujarat")) {
                return "GJ";
            } else if (state.equalsIgnoreCase("Haryana")) {
                return "HR";
            } else if (state.equalsIgnoreCase("Himachal Pradesh")) {
                return "HP";
            } else if (state.equalsIgnoreCase("Jammu & Kashmir")) {
                return "JK";
            } else if (state.equalsIgnoreCase("Karnataka")) {
                return "KA";
            } else if (state.equalsIgnoreCase("Kerala")) {
                return "KL";
            } else if (state.equalsIgnoreCase("Lakshadweep")) {
                return "LD";
            } else if (state.equalsIgnoreCase("Madhya Pradesh")) {
                return "MP";
            } else if (state.equalsIgnoreCase("Maharashtra")) {
                return "MH";
            } else if (state.equalsIgnoreCase("Manipur")) {
                return "MN";
            } else if (state.equalsIgnoreCase("Meghalaya")) {
                return "ML";
            } else if (state.equalsIgnoreCase("Mizoram")) {
                return "MZ";
            } else if (state.equalsIgnoreCase("Nagaland")) {
                return "NL";
            } else if (state.equalsIgnoreCase("Orissa")) {
                return "OR";
            } else if (state.equalsIgnoreCase("Odisha")) {
                return "OD";
            } else if (state.equalsIgnoreCase("Puducherry")) {
                return "PY";
            } else if (state.equalsIgnoreCase("Punjab")) {
                return "PB";
            } else if (state.equalsIgnoreCase("Rajasthan")) {
                return "RJ";
            } else if (state.equalsIgnoreCase("Sikkim")) {
                return "SK";
            } else if (state.equalsIgnoreCase("Tamil Nadu")) {
                return "TN";
            } else if (state.equalsIgnoreCase("Telangana")) {
                return "TG";
            } else if (state.equalsIgnoreCase("Tripura")) {
                return "TR";
            } else if (state.equalsIgnoreCase("Uttar Pradesh")) {
                return "UP";
            } else if (state.equalsIgnoreCase("Uttarakhand (Uttranchal)")) {
                return "UK";
            } else if (state.equalsIgnoreCase("West Bengal")) {
                return "WB";
            } else {
                return "BR";
            }
        } catch (Exception e) {
            return "BR";
        }
    }

    //For refresh token
    private String refreshToken(String type) {
        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v87")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                            refreshTokenEncodedURL(_token, encodedUrl, type);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                            hideLoader();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        Toast.makeText(MatmOptionActivity.this, "Please try again..check issue regarding token ", Toast.LENGTH_LONG).show();
                    }
                });
        return _token;
    }

    private void refreshTokenEncodedURL(String tokenStr, String encodedUrl, String type) {
        try {
            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                               // hideLoader();
                                JSONObject respObj = new JSONObject(response.toString());
                                _token = respObj.getString("token");
                                _admin = respObj.getString("adminName");
                                if (!_token.equalsIgnoreCase("")) {

                                    //-------  For MATM1 -------

                                    if (type.equalsIgnoreCase("MATM1")) {

                                        boolean installed = appInstalledOrNot("com.matm.matmservice_1");
                                        if (installed) {
                                            if (strTransType.equalsIgnoreCase("Cash Withdrawal")) {
                                                apiCalling();
                                            } else if (strTransType.equalsIgnoreCase("Balance Enquiry")) {
                                                balanceEnquiryApiCalling();
                                            }

                                        } else {
                                            hideLoader();
                                            try {
                                                showAlert(MatmOptionActivity.this);
                                                System.out.println("App is not installed on your phone");
                                            } catch (Exception e) {

                                            }

                                        }

                                    } else {

                                        //-------- For MATM2 ----------

                                        callMatm2(_token);


                                    }

                                } else {
                                    hideLoader();
                                    Toast.makeText(MatmOptionActivity.this, "Error in response of token !", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            System.out.println("ERROR: " + anError.getErrorDetail());
                            hideLoader();
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public void redirectToPlayStore() {
        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.matm.matmservice_1&hl=en_US");
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=com.matm.matmservice_1&hl=en_US")));
        }
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void checkRequestCode(String status, String message, MicroAtmResponse microAtmResponse) {
        hideLoader();
        if (status != null && !status.matches("")) {
            authentication = microAtmResponse.getAuthentication();
            encData = microAtmResponse.getEncData();
            String trans_type = "";
            boolean cash_withdrawal = true;

            if (strTransType.equalsIgnoreCase("Cash Withdrawal")) {
                cash_withdrawal = true;
                trans_type = "cash";
            } else if (strTransType.equalsIgnoreCase("Balance Enquiry")) {
                cash_withdrawal = false;
                trans_type = "balance";
            }

            PackageManager manager = getPackageManager();
            Intent intent = manager.getLaunchIntentForPackage("com.matm.matmservice_1");
            intent.putExtra("RequestData", encData);
            intent.putExtra("HeaderData", authentication);
            intent.putExtra("ReturnTime", 5);
            intent.putExtra("IS_PAIR_DEVICE", false);
            intent.putExtra("Flag", "transaction");
            intent.putExtra("TransactionType", trans_type);
            intent.putExtra("client_id", "");
            intent.setFlags(0);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            startActivityForResult(intent, 1);
            //finish();

        } else {
            if (!isFinishing()) {
                Util.showAlertFinish(MatmOptionActivity.this, getResources().getString(R.string.fail_error), message);
            }
        }
    }

    @Override
    public void showFeature(ArrayList<UserInfoModel.userFeature> userFeatures) {

    }

    @Override
    public void checkEmptyFields() {

    }

    @Override
    public void showLoader() {
        try {
            if (!isFinishing() && !isDestroyed()) {
                dialog = new ProgressDialog(MatmOptionActivity.this);
                dialog.setMessage("please wait..");
                dialog.setCancelable(false);
                dialog.show();
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void hideLoader() {
        try {
            if (!isFinishing() && !isDestroyed()) {
            if (dialog.isShowing()) {
            dialog.dismiss();
             }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    public void apiCalling() {
        if (mylocation != null) {
            lat = String.valueOf(mylocation.getLatitude());
            lng = String.valueOf(mylocation.getLongitude());
        }
        microAtmRequestModel = new MicroAtmRequestModel(transactionAmount, getServiceID(),lat+","+lng);
        microAtmPresenter.performRequestData(_token, microAtmRequestModel);
    }

    public void balanceEnquiryApiCalling() {
        if (mylocation != null) {
            lat = String.valueOf(mylocation.getLatitude());
            lng = String.valueOf(mylocation.getLongitude());
        }
        microAtmRequestModel = new MicroAtmRequestModel(null,getServiceID(),lat+","+lng);
        microAtmPresenter.performRequestData(_token, microAtmRequestModel);
    }

    public void showAlert(Context context) {
        try {

            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(context);
            }
            alertbuilderupdate.setCancelable(false);
            String message = "Please download the MATM service app from the playstore.";
            alertbuilderupdate.setTitle("Alert")
                    .setMessage(message)
                    .setPositiveButton("Download Now", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            redirectToPlayStore();
                        }
                    })
                    .setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            dialog.dismiss();
                        }
                    });
//                    .show();
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();

        } catch (Exception e) {

        }

    }



/*    public void showLoader() {
        try {
            if (!isFinishing() && !isDestroyed()) {
                dialog = new ProgressDialog(MatmOptionActivity.this);
                dialog.setMessage("please wait..");
                dialog.setCancelable(false);
                dialog.show();
            }
        } catch (Exception e) {

        }
    }

    public void hideLoader() {


        try {
            //if (!isFinishing() && !isDestroyed()) {
               // if (dialog.isShowing()) {
                    dialog.dismiss();
               // }
            //}
        } catch (Exception e) {
            e.printStackTrace();

        }
    }*/




    public String getServiceID() {
        String clientRefID = "";
        if (strTransType.equalsIgnoreCase("Cash Withdrawal")) {
            clientRefID = Constants.SERVICE_MICRO_CW;
        } else if (strTransType.equalsIgnoreCase("Balance Enquiry")) {
            clientRefID = Constants.SERVICE_MICRO_BE;
        }
        return clientRefID;
    }

    private void callMatm2(String _token) {
        try {
            hideLoader();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                    Toast.makeText(getApplicationContext(), "Please Grant all the permissions", Toast.LENGTH_LONG).show();
                } else {
                    sendDataToService();
                }
            } else {
                sendDataToService();
            }



        } catch (Exception e) {



        }
    }

    private void sendDataToService() {

        //for cash withdrawal------
        if (strTransType.equalsIgnoreCase("Cash Withdrawal")) {
            SdkConstants.transactionType = SdkConstants.cashWithdrawal;
            SdkConstants.transactionAmount = transactionAmount;
            SdkConstants.applicationType = "CORE";
            SdkConstants.tokenFromCoreApp = _token;
            SdkConstants.userNameFromCoreApp = _userName;
            SdkConstants.paramB = "";
            SdkConstants.paramC = "";
            SdkConstants.loginID = "";
            matm_transaction_intent = true;
            Intent intent = new Intent(MatmOptionActivity.this, PosServiceActivity.class);
            startActivity(intent);
            finish();

        //for balance enquiry------
        } else if (strTransType.equalsIgnoreCase("Balance Enquiry")) {

            SdkConstants.transactionType = SdkConstants.balanceEnquiry;
            SdkConstants.transactionAmount = "0";
            SdkConstants.applicationType = "CORE";
            SdkConstants.tokenFromCoreApp = _token;
            SdkConstants.userNameFromCoreApp = _userName;
            SdkConstants.paramB = "";
            SdkConstants.paramC = "";
            SdkConstants.loginID = "";
            matm_transaction_intent = true;
            Intent intent = new Intent(MatmOptionActivity.this, PosServiceActivity.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, "Choose transaction type.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null & resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Log.d("TAG", "onActivityResult: " + data.getExtras().toString());
                responseData = data.getStringExtra("matm1data");
                if (responseData != null) {
                    //onBackPressed();
                    JSONObject jresponse = null;
                    if (responseData != null) {
                        if (isJSONValid(responseData)) {
                            try{

                                jresponse = new JSONObject(responseData);
                                Intent intent = new Intent(MatmOptionActivity.this, Matm1TransactionStatusActivity.class);
                                if (jresponse.has("BalanceEnquiryStatus")) {
                                    intent.putExtra("BalanceEnquiryStatus", jresponse.getString("BalanceEnquiryStatus"));
                                    intent.putExtra("AccountNo", jresponse.getString("AccountNo"));
                                } else {
                                    intent.putExtra("TxnStatus", jresponse.getString("TxnStatus"));
                                    intent.putExtra("TxnAmount", jresponse.getString("TxnAmt"));
                                }
                                intent.putExtra("RRN", jresponse.getString("RRN"));
                                intent.putExtra("CardNumber", jresponse.getString("CardNumber"));
                                intent.putExtra("AvailableBalance", jresponse.getString("AvailableBalance"));
                                intent.putExtra("TransactionDatetime", jresponse.getString("TransactionDatetime"));
                                intent.putExtra("TerminalID", jresponse.getString("TerminalID"));
                                startActivity(intent);
                                hideLoader();
                                finish();

                            }catch (Exception e){

                            }

                        } else {
                            Intent intent = new Intent(MatmOptionActivity.this, ErrorMatm1Activity.class);
                            intent.putExtra("response", responseData);
                            startActivity(intent);
                            hideLoader();
                            finish();
                        }
                    }

                }

            }
        }
    }


    public boolean isJSONValid(String response) {
        try {
            new JSONObject(response);
        } catch (JSONException ex) {
            return false;
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void checkPermission() {
        if (Build.VERSION.SDK_INT > 15) {
            final String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};

            final List<String> permissionsToRequest = new ArrayList<>();
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(MatmOptionActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
                    permissionsToRequest.add(permission);
                }
            }
            if (!permissionsToRequest.isEmpty()) {

                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CA_PERMISSIONS);

            } else {
                getMyLocation();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        int permissionLocation = ContextCompat.checkSelfPermission(MatmOptionActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
        } else {
            Toast.makeText(this, "Please accept the location permission", Toast.LENGTH_SHORT).show();

            if (!isFinishing()) {
                finish();
            }
        }
    }

    private void getMyLocation() {
        if (googleApiClient != null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(MatmOptionActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
                    PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                            .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat.checkSelfPermission(MatmOptionActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        mylocation = LocationServices.FusedLocationApi
                                                .getLastLocation(googleApiClient);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(MatmOptionActivity.this,
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            hideLoader();
            submitMatm.setEnabled(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                checkPermission();
            }
        } catch (Exception e) {
            hideLoader();

        }

    }

    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }
}