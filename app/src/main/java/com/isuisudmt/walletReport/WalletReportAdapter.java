package com.isuisudmt.walletReport;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.Util;
import com.paxsz.easylink.model.AppSelectResponse;

import java.util.ArrayList;
import java.util.Iterator;

public class WalletReportAdapter extends RecyclerView.Adapter<WalletReportAdapter.ViewHolder> implements Filterable {
    private Context context;
    ArrayList<WalletReportModel> reportModels;
    ArrayList<WalletReportModel> reportModelsFilter;

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView accNo;
        TextView amount_balance_txt;
        TextView amount_transact_txt;
        TextView api_comment;
        TextView bankName;
        TextView dateTime;
        TextView operation_perf;
        TextView previous_amount;
        TextView route;
        TextView statusTextView;
        TextView tnxType;

        public ViewHolder(View itemView) {
            super(itemView);
            this.dateTime = (TextView) itemView.findViewById(R.id.dateTime);
            this.statusTextView = (TextView) itemView.findViewById(R.id.statusTextView);
            this.bankName = (TextView) itemView.findViewById(R.id.bankName);
            this.amount_transact_txt = (TextView) itemView.findViewById(R.id.amount_transact_txt);
            this.amount_balance_txt = (TextView) itemView.findViewById(R.id.amount_balance_txt);
            this.previous_amount = (TextView) itemView.findViewById(R.id.amount);
            this.accNo = (TextView) itemView.findViewById(R.id.accNo);
            this.tnxType = (TextView) itemView.findViewById(R.id.tnxType);
            this.route = (TextView) itemView.findViewById(R.id.route);
            this.api_comment = (TextView) itemView.findViewById(R.id.api_comment);
            this.operation_perf = (TextView) itemView.findViewById(R.id.operation_perf);
        }
    }

    public WalletReportAdapter(ArrayList<WalletReportModel> reportModels2, Context context2) {
        this.reportModels = reportModels2;
        this.reportModelsFilter = reportModels2;
        this.context = context2;
    }

    public Filter getFilter() {
        return new Filter() {
            /* access modifiers changed from: protected */
            public FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    WalletReportAdapter walletReportAdapter = WalletReportAdapter.this;
                    walletReportAdapter.reportModelsFilter = walletReportAdapter.reportModels;
                } else {
                    ArrayList<WalletReportModel> filteredList = new ArrayList<>();
                    Iterator it = WalletReportAdapter.this.reportModels.iterator();
                    while (it.hasNext()) {
                        WalletReportModel row = (WalletReportModel) it.next();
                        if (row.getApiTId().toLowerCase().contains(charString.toLowerCase()) || row.getApiTId().contains(charSequence)) {
                            filteredList.add(row);
                        }
                        if (String.valueOf(row.getId()).toLowerCase().contains(charString.toLowerCase()) || String.valueOf(row.getId()).contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    WalletReportAdapter.this.reportModelsFilter = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = WalletReportAdapter.this.reportModelsFilter;
                return filterResults;
            }

            /* access modifiers changed from: protected */
            public void publishResults(CharSequence charSequence, FilterResults filterResults) {
                WalletReportAdapter.this.reportModelsFilter = (ArrayList) filterResults.values;
                WalletReportAdapter.this.notifyDataSetChanged();
            }
        };
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.wallet_report_row, parent, false));
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        String str = "₹";
        try {
            holder.itemView.setTag(this.reportModelsFilter.get(position));
            WalletReportModel pu = (WalletReportModel) this.reportModelsFilter.get(position);
            TextView textView = holder.tnxType;
            StringBuilder sb = new StringBuilder();
            sb.append("Txn ID: ");
            sb.append(pu.getId());
            textView.setText(sb.toString());
            TextView textView2 = holder.operation_perf;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Operation : ");
            sb2.append(pu.getOperationPerformed());
            textView2.setText(sb2.toString());
            TextView textView3 = holder.previous_amount;
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append(pu.getPreviousAmount());
            textView3.setText(String.valueOf(sb3.toString()));
            TextView textView4 = holder.amount_transact_txt;
            StringBuilder sb4 = new StringBuilder();
            sb4.append(str);
            sb4.append(pu.getAmountTransacted());
            textView4.setText(String.valueOf(sb4.toString()));
            TextView textView5 = holder.amount_balance_txt;
            StringBuilder sb5 = new StringBuilder();
            sb5.append(str);
            sb5.append(pu.getBalanceAmount());
            textView5.setText(String.valueOf(sb5.toString()));
            TextView textView6 = holder.dateTime;
            StringBuilder sb6 = new StringBuilder();
            sb6.append("Date: ");
            sb6.append(Util.getDateFromTime(pu.getCreatedDate()));
            textView6.setText(sb6.toString());
            holder.statusTextView.setText(pu.getStatus());
            if (pu.getStatus().equalsIgnoreCase(AppSelectResponse.SUCCESS)) {
                holder.statusTextView.setTextColor(ContextCompat.getColor(this.context, R.color.color_report_green));
            } else {
                holder.statusTextView.setTextColor(ContextCompat.getColor(this.context, R.color.red));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getItemCount() {
        return this.reportModelsFilter.size();
    }
}
