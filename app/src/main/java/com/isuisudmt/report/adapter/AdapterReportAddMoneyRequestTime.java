package com.isuisudmt.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.FetchraisedRequestModel;
import com.isuisudmt.R;
import com.isuisudmt.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class AdapterReportAddMoneyRequestTime extends RecyclerView.Adapter<AdapterReportAddMoneyRequestTime.ViewHolder> implements Filterable {
    private int lastSelectedPosition = -1;
    private Context context;
    private List<FetchraisedRequestModel> finoTransactionReports;
    private List<FetchraisedRequestModel> finoTransactionReportsFiltered;
    TextView fragAmount, fragTotal;

    public AdapterReportAddMoneyRequestTime(Context context, ArrayList<FetchraisedRequestModel> finoTransactionReports, TextView fragAmount, TextView fragTotal) {
        this.context = context;
        this.finoTransactionReports = finoTransactionReports;
        this.finoTransactionReportsFiltered = finoTransactionReports;
        this.fragTotal = fragTotal;
        this.fragAmount = fragAmount;

        Collections.reverse(finoTransactionReports);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FetchraisedRequestModel current = finoTransactionReportsFiltered.get(position);

        //holder.updated_date.setText("Date: " + Util.getDateTime(String.valueOf(current.getApproveOrDeclineTime())));
        try {
            SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:SSS");
            Date dateValue = input.parse(current.getApproveOrDeclineTime());
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy  h:mm a");
            holder.updated_date.setText("Date: " + formatter.format(dateValue));

        } catch (ParseException e) {
            holder.updated_date.setText("Date: N/A");

            e.printStackTrace();
        }


        holder.status.setText(current.getStatus());
        holder.transactionId.setText(new StringBuilder().append("Transaction Id: ").append(String.valueOf(current.getId())).toString());
        if (current.getTransferType().equalsIgnoreCase("")) {
            holder.upiId.setText("Transfer type: " + "N/A");
        } else {
            holder.upiId.setText(new StringBuilder().append("Transfer type: ").append(current.getTransferType()).toString());

        }
        holder.operator_performed.setText("Request to: " + current.getRequestTo());
        //holder.refNo.setText("Reference No: "+current.getReferenceNo());
        // holder.upiTransID.setText("Approve/Decline: "+current.getApproveOrDeclineTime());

        if (current.getBankRefId().equalsIgnoreCase("") || current.getBankRefId().equals("null") || current.getBankRefId().equals(null))
            holder.apiTid.setText("Bank Ref ID: N/A");
        else
            holder.apiTid.setText("Bank Ref ID: " + current.getBankRefId());


        holder.apiComment.setText("Request From: " + current.getRequestFrom());

        if (current.getDepositedBankName().equalsIgnoreCase("") || current.getDepositedBankName().equals("null") || current.getDepositedBankName().equals(null))
            holder.bank_name.setText("Bank Name: N/A");
        else
            holder.bank_name.setText("Bank Name: " + current.getDepositedBankName());

        if (current.getAmount().equalsIgnoreCase("")) {
            holder.amount_transacted_recharge.setText("Amount: N/A");
        } else
            holder.amount_transacted_recharge.setText("Amount: ₹" + current.getAmount());

        holder.depositedDate.setText("Deposited Date: " + Util.getDateTime(String.valueOf(current.getDepositedDate())));
        holder.requestedTime.setText("Requested Time: " + Util.getDateTime(String.valueOf(current.getRequestedTime())));


        if (current.getStatus().equalsIgnoreCase("APPROVED")) {
            holder.status.setTextColor(ContextCompat.getColor(this.context, R.color.color_report_green));
        } else {
            holder.status.setTextColor(ContextCompat.getColor(this.context, R.color.red));
        }
    }

    @Override
    public int getItemCount() {
        return finoTransactionReportsFiltered.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView amount_transacted_recharge;
        public TextView upiId;
        public TextView operator_performed;
        public TextView status;
        public TextView transactionId;
        //public TextView refNo;
        public TextView updated_date;
        public TextView apiTid, apiComment, upiTransID, bank_name, depositedDate, requestedTime;

        public ViewHolder(View itemView) {
            super(itemView);
            updated_date = itemView.findViewById(R.id.updated_date);
            status = itemView.findViewById(R.id.status);
            transactionId = itemView.findViewById(R.id.transactionId);
            amount_transacted_recharge = itemView.findViewById(R.id.amount_transacted);
            upiId = itemView.findViewById(R.id.upiId);
            operator_performed = itemView.findViewById(R.id.operation_performed);
            //refNo =  itemView.findViewById(R.id.refNo);
            upiTransID = itemView.findViewById(R.id.upiTransID);
            apiTid = itemView.findViewById(R.id.apiTid);
            apiComment = itemView.findViewById(R.id.apiComment);
            bank_name = itemView.findViewById(R.id.bank_name);

            requestedTime = itemView.findViewById(R.id.requestedTime);
            depositedDate = itemView.findViewById(R.id.depositedDate);

        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    finoTransactionReportsFiltered = finoTransactionReports;
                } else {
                    List<FetchraisedRequestModel> filteredList = new ArrayList<>();
                    for (FetchraisedRequestModel row : finoTransactionReports) {

                        if (row.getDepositedBankName().toLowerCase().contains(charString.toLowerCase()) || row.getDepositedBankName().contains(charSequence) ||
                                row.getTransferType().toLowerCase().contains(charString.toLowerCase()) || row.getTransferType().contains(charSequence) ||
                                row.getId().toLowerCase().contains(charString.toLowerCase()) || row.getId().contains(charSequence)) {
                            filteredList.add(row);
                        }

                    }
                    finoTransactionReportsFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = finoTransactionReportsFiltered;

                getTotalAmount();


                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                finoTransactionReportsFiltered = (ArrayList<FetchraisedRequestModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
/*
    public List<FetchraisedRequestModel> getItems() {
        return finoTransactionReportsFiltered;
    }

    public double getTotalAmount() {
        double totalAmount = 0;
        for (int i = 0; i < finoTransactionReportsFiltered.size(); i++) {
            if (finoTransactionReportsFiltered.get(i).getAmount().equals("N/A") || finoTransactionReportsFiltered.get(i).getAmount().equals("null") ||
                    finoTransactionReportsFiltered.get(i).getAmount().toString().equals(null)) {
                totalAmount += 0;
            } else {
                totalAmount += Double.parseDouble(String.valueOf(finoTransactionReportsFiltered.get(i).getAmount()));
            }
        }

        return totalAmount;
    }*/

    public double getTotalAmount() {
        double totalAmount = 0;
        for (int i = 0; i < finoTransactionReportsFiltered.size(); i++) {
            if (finoTransactionReportsFiltered.get(i).getAmount().equals("N/A") || finoTransactionReportsFiltered.get(i).getAmount().equals("null") ||
                    finoTransactionReportsFiltered.get(i).getAmount().toString().equals(null)) {
                totalAmount += 0;
            } else {
                totalAmount += Double.parseDouble(String.valueOf(finoTransactionReportsFiltered.get(i).getAmount()));
            }
        }

        fragAmount.setText(context.getResources().getString(R.string.toatlamountacitvity) + totalAmount);
        fragTotal.setText("Entries: " + finoTransactionReportsFiltered.size());

        return totalAmount;
    }

}
