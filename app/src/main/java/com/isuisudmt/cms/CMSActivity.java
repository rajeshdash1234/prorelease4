package com.isuisudmt.cms;

import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.isuisudmt.CustomThemes;
import com.isuisudmt.R;
import com.isuisudmt.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import static com.isuisudmt.SessionManager.userName;

public class CMSActivity extends AppCompatActivity {
    ProgressBar progressV;
    ImageView btnOk;
    EditText pincodeTxt;
    String pinData="";
    private FirebaseAnalytics firebaseAnalytics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);
        setContentView(R.layout.activity_cms);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        progressV = findViewById(R.id.progressV);
        btnOk = findViewById(R.id.btnOk);
        pincodeTxt = findViewById(R.id.pincodeTxt);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pinData = pincodeTxt.getText().toString();
                if (!pinData.isEmpty()) {
                    setFBAnalytics("CMS_ACTIVITY_OK", userName);
                    callPinValidation(pinData);
                    pincodeTxt.setError(null);
                }
                else {
                    pincodeTxt.setError("Pin required !");
                }

            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.pinValidate));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }




    //================

    private void callPinValidation( String pinCode) {
        progressV.setVisibility(View.VISIBLE);

        JSONObject obj = new JSONObject();
        try {
            obj.put("number",pinCode);
            AndroidNetworking.post("https://itpl.iserveu.tech/generate/v52")
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----"+key);
                                byte[] data = Base64.decode(key,Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                System.out.println(">>>>-----"+encodedUrl);
                                callEncriptedPinValidation(encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            progressV.setVisibility(View.GONE);
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void callEncriptedPinValidation(String encodedUrl) {
        AndroidNetworking.get(encodedUrl)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            progressV.setVisibility(View.GONE);
                            JSONObject obj = new JSONObject(response.toString());
                            String statusCode = obj.getString("statusCode");
                            if(statusCode.equalsIgnoreCase("0")){
                                String statusDescription = obj.getString("statusDesc");
                                //Toast.makeText(CMSActivity.this,statusDescription,Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(CMSActivity.this,CashCollectionActivity.class);
                                intent.putExtra("pinData",pinData);
                                startActivity(intent);
                            }else{
                                Toast.makeText(CMSActivity.this,"Failure",Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {

                        String responseStr = anError.getErrorBody();
                        try {
                            JSONObject resobj = new JSONObject(responseStr);
                            String errorStr = resobj.getString("statusDesc");
                            //Toast.makeText(CMSActivity.this, errorStr, Toast.LENGTH_SHORT).show();
                            Util.showAlert(CMSActivity.this,"Info!",errorStr);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressV.setVisibility(View.GONE);
                    }
                });

    }
    private void setFBAnalytics(String propertyKey, String propertyValue) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, getPackageName());
        //Logs an app event.
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        //Sets whether analytics collection is enabled for this app on this device.
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
        firebaseAnalytics.setSessionTimeoutDuration(500);
        firebaseAnalytics.setDefaultEventParameters(bundle);
        //Sets the user ID property.
        firebaseAnalytics.setUserId(getPackageName());
        //Sets a user property to a given value.
        firebaseAnalytics.setUserProperty(propertyKey, propertyValue);
    }

}
