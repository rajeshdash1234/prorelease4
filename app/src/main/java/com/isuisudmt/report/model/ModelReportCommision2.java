package com.isuisudmt.report.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ModelReportCommision2 implements Serializable {
    @SerializedName("Id")
    @Expose
    private String id;

    @SerializedName("relationalId")
    @Expose
    private String relationalId;

    @SerializedName("RelationalAmount")
    @Expose
    private String relationalAmount;
    @SerializedName("previousAmount")
    @Expose
    private String previousAmount;
    @SerializedName("amountTransacted")
    @Expose
    private String amountTransacted;
    @SerializedName("balanceAmount")
    @Expose
    private String balanceAmount;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("creditedUser")
    @Expose
    private String creditedUser;
    @SerializedName("distributerName")
    @Expose
    private String distributerName;
    @SerializedName("masterName")
    @Expose
    private String masterName;
    @SerializedName("transactingUser")
    @Expose
    private String transactingUser;
    @SerializedName("RelationalOperation")
    @Expose
    private String relationalOperation;
    @SerializedName("API")
    @Expose
    private String aPI;
    @SerializedName("createdDate")
    @Expose
    private Long createdDate;
    @SerializedName("updatedDate")
    @Expose
    private Long updatedDate;
    @SerializedName("type")
    @Expose
    private String type;

    public String getRelationalId() {
        return relationalId;
    }

    public void setRelationalId(String relationalId) {
        this.relationalId = relationalId;
    }

    public String getRelationalAmount() {
        return relationalAmount;
    }

    public void setRelationalAmount(String relationalAmount) {
        this.relationalAmount = relationalAmount;
    }

    public String getPreviousAmount() {
        return previousAmount;
    }

    public void setPreviousAmount(String previousAmount) {
        this.previousAmount = previousAmount;
    }

    public String getAmountTransacted() {
        return amountTransacted;
    }

    public void setAmountTransacted(String amountTransacted) {
        this.amountTransacted = amountTransacted;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreditedUser() {
        return creditedUser;
    }

    public void setCreditedUser(String creditedUser) {
        this.creditedUser = creditedUser;
    }

    public String getDistributerName() {
        return distributerName;
    }

    public void setDistributerName(String distributerName) {
        this.distributerName = distributerName;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getTransactingUser() {
        return transactingUser;
    }

    public void setTransactingUser(String transactingUser) {
        this.transactingUser = transactingUser;
    }

    public String getRelationalOperation() {
        return relationalOperation;
    }

    public void setRelationalOperation(String relationalOperation) {
        this.relationalOperation = relationalOperation;
    }

    public String getaPI() {
        return aPI;
    }

    public void setaPI(String aPI) {
        this.aPI = aPI;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public Long getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Long updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
