package com.isuisudmt.wallet;

import android.util.Base64;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isuisudmt.matm.MicroReportModel;
import com.isuisudmt.matm.MicroReportResponse;
import com.matm.matmsdk.aepsmodule.utils.AEPSAPIService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;


public class WalletReportPresenter implements WalletReportContract.UserActionsListener {

    /**
     * Initialize ReportContractView
     */
    private WalletReportContract.View microReportContractView;
    private AEPSAPIService aepsapiService;
    private ArrayList<MicroReportModel> microReportModelArrayList;

    /**
     * Initialize ReportPresenter
     */
    public WalletReportPresenter(WalletReportContract.View microReportContractView) {
        this.microReportContractView = microReportContractView;
    }

    @Override
    public void loadReports(final String fromDate, final String toDate, final String token, final String transactionType) {
        if (fromDate != null && !fromDate.matches("") && toDate != null && !toDate.matches("")) {
            microReportContractView.showLoader();
            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }
            AndroidNetworking.get("https://itpl.iserveu.tech/generate/v95")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                encryptedReport(fromDate, toDate, token, encodedUrl, transactionType);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }


                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });


        } else {
            microReportContractView.emptyDates();
        }
    }


    public void encryptedReport(String fromDate, String toDate, String token, String encodedUrl, String type) {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("transactionType", type);
            jsonObject.put("fromDate", fromDate);
            jsonObject.put("toDate", toDate);


            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization", token)
                    .addJSONObjectBody(jsonObject)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                JSONArray jsonArray = obj.getJSONArray("BQReport");

                                MicroReportResponse reportResponse = new MicroReportResponse();
                                ArrayList<MicroReportModel> reportModels = new ArrayList<>();

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    MicroReportModel reportModel = new MicroReportModel();
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);


                                    if (type.equalsIgnoreCase("WALLET2")) {
                                        reportModel.setId(jsonObject1.getString("Id"));

                                        String previousAmount = jsonObject1.getString("previousBalance");
                                        if (!previousAmount.equalsIgnoreCase("null")) {
                                            reportModel.setPreviousAmount(previousAmount);
                                        } else {
                                            reportModel.setPreviousAmount("N/A");
                                        }

                                        String balanceAmount = jsonObject1.getString("currentBalance");
                                        if (!balanceAmount.equalsIgnoreCase("null")) {
                                            reportModel.setBalanceAmount(balanceAmount);
                                        } else {
                                            reportModel.setBalanceAmount("N/A");
                                        }

                                        String amountTransacted = jsonObject1.getString("amountTransacted");
                                        if (!amountTransacted.equalsIgnoreCase("null")) {
                                            reportModel.setAmountTransactedValue(Float.valueOf(amountTransacted));
                                        } else {
                                            reportModel.setAmountTransactedValue(0.0f);
                                        }
                                        reportModel.setStatus(jsonObject1.getString("status"));
                                        reportModel.setTransactionMode(jsonObject1.getString("transactionType"));


                                        reportModel.setUserName(jsonObject1.getString("userName"));
                                        reportModel.setDistributerName(jsonObject1.getString("distributerName"));
                                        reportModel.setMasterName(jsonObject1.getString("masterName"));

                                        reportModel.setCreatedDate(String.valueOf(jsonObject1.getLong("createdDate")));
                                        reportModel.setUpdatedDate(String.valueOf(jsonObject1.getLong("updatedDate")));
                                        reportModels.add(reportModel);
                                    } else {
                                        if (type.equalsIgnoreCase("WALLET")) {


                                            reportModel.setId(jsonObject1.getString("Id"));

                                            String previousAmount = jsonObject1.getString("previousAmount");
                                            if (!previousAmount.equalsIgnoreCase("null")) {
                                                reportModel.setPreviousAmount(previousAmount);
                                            } else {
                                                reportModel.setPreviousAmount("N/A");
                                            }

                                            String balanceAmount = jsonObject1.getString("balanceAmount");
                                            if (!balanceAmount.equalsIgnoreCase("null")) {
                                                reportModel.setBalanceAmount(balanceAmount);
                                            } else {
                                                reportModel.setBalanceAmount("N/A");
                                            }

                                            String amountTransacted = jsonObject1.getString("amountTransacted");
                                            if (!amountTransacted.equalsIgnoreCase("null")) {
                                                reportModel.setAmountTransactedValue(Float.valueOf(amountTransacted));
                                            } else {
                                                reportModel.setAmountTransactedValue(0.0f);
                                            }

                                            reportModel.setApiTId(jsonObject1.getString("apiTId"));
                                            reportModel.setStatus(jsonObject1.getString("status"));
                                            reportModel.setTransactionMode(jsonObject1.getString("operationPerformed"));
                                            reportModel.setOperationPerformed(jsonObject1.getString("operationPerformed"));


                                            reportModel.setUserName(jsonObject1.getString("userName"));
                                            reportModel.setDistributerName(jsonObject1.getString("distributerName"));
                                            reportModel.setMasterName(jsonObject1.getString("masterName"));

                                            reportModel.setCreatedDate(String.valueOf(jsonObject1.getLong("createdDate")));
                                            reportModel.setUpdatedDate(String.valueOf(jsonObject1.getLong("updatedDate")));
                                            reportModels.add(reportModel);
                                        }

                                    }


                                }
                                Collections.reverse(reportModels);
                                reportResponse.setmATMTransactionReport(reportModels);


                                if (reportResponse != null && reportResponse.getmATMTransactionReport() != null) {
                                    ArrayList<MicroReportModel> result = new ArrayList<>();
                                    result.clear();
                                    result = reportResponse.getmATMTransactionReport();
                                    double totalAmount = 0;
                                    for (int i = 0; i < result.size(); i++) {
                                        totalAmount += Double.parseDouble(String.valueOf(result.get(i).getAmountTransactedValue()));
                                    }
                                    microReportContractView.reportsReady(result, String.valueOf(totalAmount));
                                }
                                microReportContractView.hideLoader();
                                microReportContractView.showReports();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                microReportContractView.hideLoader();
                                microReportContractView.showReports();
                            }


                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.getErrorDetail();
                            microReportContractView.hideLoader();
                            microReportContractView.showReports();
                        }
                    });


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}
